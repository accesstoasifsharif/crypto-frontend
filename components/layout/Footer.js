import React from 'react'

function Footer() {
  return (
   <>
  
{/* footer */}
<footer>
<div className="container-fluid">
    <div className="row  right-register-bg">
        <div className="col-md-3  p-3 text-center text-md-left">
            <h2 className="register-faq">FAQ</h2>
            <ul className="quick-links-register mt-3">
                <li><a href="#">How do I access it?</a></li>
                <li><a href="#">How do editions function?</a></li>
                <li><a href="#">Can I Withdraw Coins?</a></li>
                <li><a href="#">How I Access my account?</a></li>
                <li><a href="#">How bet will proceed?</a></li>
                <li><a href="#">How Troubleshoot account?</a></li>
            </ul>
        </div>
        <div className="col-md-3 p-3 text-center text-md-left">
            <h2 className="register-contact">CONTACT </h2>
            <p className="quick-links-register mt-3">
                <a href="mailto:opsportsbook10@gmail.com">Contact Us</a>
            </p>
        </div>

        <div className="col-md-3 p-3 text-center text-md-left">
            <h2 className="register-twitter">
                <a className="register-twitter" href="https://twitter.com/OpSportsBook" target="_blank">
                    TWITTER
                </a>
            
            </h2>
        </div>
        <div className="col-md-3  p-3 text-center text-md-left">
            <h2 className="register-discord">DISCORD</h2>
        </div>
    </div>
    <div className="copyright_div">
        <ul>
            <li>Copy Right OpSportsbook-2022</li>
            <li>All Rights Reserved</li>
        </ul>

        {/* <img  className="plus_icon_img" src="/images/plus-circle.svg" /> */}
       
    </div>
</div>

</footer>
   
   
   
   
   
   </>
  )
}

export default Footer;