import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from 'next/link';
import $ from 'jquery';
import UserAuth from '../../user_auth';
let UserCheckAuthObj = new UserAuth();


 function Navbar(/* {data} */) {
    const router = useRouter();
    const [op_user, set_op_user] = useState(null);

    useEffect(() => {
        if(!router.isReady) return;
        var user_from_local_storage = getUserFromLocalStorage();
        set_op_user(user_from_local_storage);
        var url = window.location.href; 
        // now grab every link from the navigation
        setTimeout(function(){
            $('div#navbarSupportedContent ul>li a').each(function(){
                //console.log(this.href);
                if(this.href == url){
                    $(this).parent().addClass('active');
                }
            });
        },500);
        
       
        
    }, [router.isReady]);
    
    function getUserFromLocalStorage(){
        return JSON.parse(localStorage.getItem('op_user')) || null;
    }
    const sidebarToggle = async event => {
        $("#navbarSupportedContent").toggleClass("show");
    }
    const adminLogout = async event => {
        UserCheckAuthObj.UserLogout({router});
    }
    const searchBar = async event => {
       $(".global_search").toggleClass("collapsed");
    }
    const searchHandler = async event => {
        event.preventDefault();
        const search = event.target.search.value;
       //localStorage.setItem("op_user_search",search);
        router.push(`/games?search=${search}`);
        //router.push(`/games`);
    }
    //console.log(op_user);
  return (
   <>

<nav className="navbar navbar-expand-lg py-3 navbar-dark bg-dark shadow-sm custom_nav sticky-top">
<div className="container-fluid flex_mob_menu">

   {/*  <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" className="navbar-toggler"><span className=""><i className="fa fa-bars text-dark" aria-hidden="true"></i></span></button> */}
   <button type="button" className="navbar-toggler" onClick={sidebarToggle}><span className=""><i className="fa fa-bars text-white" aria-hidden="true"></i></span></button>

 
    <div className="right_side hidden_in_desktop right_side_mob">
        <ul>
            <li className="search collapsed">
                <input type="text" placeholder="Search"/>
                <a></a>
            </li>
        </ul>
    </div>
    

    <div className="top_nav_flexbox">
    <Link href="/">
         <a className="navbar-brand m-0">
            
             <h1 className="logo_text">OP<span>Sportsbook</span></h1>
            
             <span className="text-uppercase font-weight-bold short_title">Decentralized Anonymous Global</span>
         </a>
    </Link>
    </div>
    {op_user !== null ? (<div id="navbarSupportedContent" className="collapse navbar-collapse sidebar-menu">
        <ul className="navbar-nav ml-auto">
            <li className="search collapsed hidden_in_mob global_search nav-item">
                <form onSubmit={searchHandler}>
                 <input type="text" id="search" name="search" placeholder="Search"/>
                 </form>
                 <a onClick={searchBar}></a>
            </li>
            <li className="right_menu_a_login nav-item"><a onClick={adminLogout} style={{cursor:"pointer"}}>Log Out</a></li>
            <li className="nav-item">
                <Link href="/" className="nav-link"><a><img src="/images/home(white).png" /> Home <span className="sr-only">(current)</span></a></Link>
            </li>
        {/*
            <li className="nav-item">
                <Link href="/bet" className="nav-link"><a><img src="/images/betting(white).png" /> My Bets</a></Link>
            </li>
        */}
            
            {/* <li className="nav-item">
                <Link href="/promotion" className="nav-link"><a><img src="/images/megaphone.png" /> Promotions</a></Link>
            </li> */}
           
            <li className="nav-item">
                <Link href="/dashboard" className="nav-link"><a><img src="/images/profile(white).png" /> Dashboard</a></Link>
            </li>
            
        </ul>
    </div>):(<div id="navbarSupportedContent" className="collapse navbar-collapse sidebar-menu">
        <ul className="navbar-nav ml-auto">
            <li className="search collapsed hidden_in_mob global_search">
                <form onSubmit={searchHandler}>
                 <input type="text" id="search" name="search" placeholder="Search"/>
                 </form>
                 <a  onClick={searchBar}></a>
            </li>
             <li className="right_menu_a_login nav-item"><Link href="/login"><a>Log In</a></Link></li>
             <li className="right_menu_a_join nav-item"><Link href="/register"><a>Join Now</a></Link></li>
             <li className="nav-item">
                <Link href="/" className="nav-link"><a><img src="/images/home(white).png" /> Home <span className="sr-only">(current)</span></a></Link>
            </li>   
        </ul>
    </div>)}


</div>
</nav>
   </>
  )
}
export default Navbar;