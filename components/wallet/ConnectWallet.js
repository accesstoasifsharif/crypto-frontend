import { WalletMultiButton as ReactUIWalletMultiButton } from "@solana/wallet-adapter-react-ui";

function ConnectWallet() {
	return <ReactUIWalletMultiButton />;
}

export default ConnectWallet;
