import { WalletAdapterNetwork, WalletError } from "@solana/wallet-adapter-base";
import {
	ConnectionProvider,
	WalletProvider,
} from "@solana/wallet-adapter-react";
import { WalletModalProvider as ReactUIWalletModalProvider } from "@solana/wallet-adapter-react-ui";
import { PhantomWalletAdapter } from "@solana/wallet-adapter-wallets";
import { clusterApiUrl } from "@solana/web3.js";
import { FC, ReactNode, useCallback, useMemo } from "react";
import {
	AutoConnectProvider,
	useAutoConnect,
} from "../wallet/AutoConnectProvider.tsx";

const WalletContextProvider = ({ children }) => {
	const { autoConnect, setAutoConnect } = useAutoConnect();

	// Can be set to 'devnet', 'testnet', or 'mainnet-beta'
	const network = WalletAdapterNetwork.Mainnet;

	// You can also provide a custom RPC endpoint
	const endpoint = useMemo(() => clusterApiUrl(network), [network]);

	// @solana/wallet-adapter-wallets includes all the adapters but supports tree shaking and lazy loading --
	// Only the wallets you configure here will be compiled into your application, and only the dependencies
	// of wallets that your users connect to will be loaded
	const wallets = useMemo(() => [new PhantomWalletAdapter()], [network]);
	return (
		<ConnectionProvider endpoint={endpoint}>
			<WalletProvider wallets={wallets} autoConnect={autoConnect}>
				<ReactUIWalletModalProvider>
					{children}
				</ReactUIWalletModalProvider>
			</WalletProvider>
		</ConnectionProvider>
	);
};

export const ContextProvider = ({ children }) => {
	return (
		<AutoConnectProvider>
			<WalletContextProvider>{children}</WalletContextProvider>
		</AutoConnectProvider>
	);
};
