import ConnectWallet from "./ConnectWallet";
import WalletBalance from "./WalletBalance";
import SwapCryptoToken from "../crypto/SwapCryptoToken";
import { useEffect, useState } from "react";

function MyWallet() {
	const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);

	useEffect(() => {
		var user_from_local_storage = getUserFromLocalStorage();
		if (
			Object.keys(user_from_local_storage).length !== 0 &&
			user_from_local_storage.constructor === Object
		) {
			setIsUserLoggedIn(true);
		}
	}, []);
	function getUserFromLocalStorage() {
		return JSON.parse(localStorage.getItem("op_user")) || {};
	}

	return (
		<div>
			{isUserLoggedIn ? (
				<>
					<ConnectWallet />
					<WalletBalance />
					<SwapCryptoToken />
				</>
			) : null}
		</div>
	);
}

export default MyWallet;
