import { useSelector, useDispatch } from "react-redux";
import { useConnection, useWallet } from "@solana/wallet-adapter-react";
import { useEffect, useState } from "react";
import * as types from "../../store/actions/types";
import { LAMPORTS_PER_SOL, PublicKey } from "@solana/web3.js";
import { getOrCreateAssociatedTokenAccount, getMint } from "@solana/spl-token";
import { roundToDecimal } from "../../src/helpers";
import axios from "axios";

function WalletBalance() {
	const walletReducer = useSelector((state) => state.wallet);
	const { connection } = useConnection();
	const { publicKey, connected, wallet } = useWallet();
	const dispatch = useDispatch();
	const opSportWallet = walletReducer.opSportWallet;

	const [isLoading, setIsLoading] = useState();

	useEffect(() => {
		var user_from_local_storage = getUserFromLocalStorage();

		if (connected) {
			getBETCBalance();
			saveUserCryptoWallets(user_from_local_storage);
		}
		if (!wallet) {
			dispatch({
				type: types.UPDATE_WALLET_BALANCE,
				payload: 0,
			});
		}
	}, [connected, wallet]);
	function getUserFromLocalStorage() {
		return JSON.parse(localStorage.getItem("op_user")) || [];
	}
	// async function getSOLBalance() {
	// 	try {
	// 		let balance = await connection.getBalance(publicKey);
	// 		let SOLbalance = balance / LAMPORTS_PER_SOL;
	// 		console.log("SOLbalance -> :", SOLbalance, "SOL");
	// 	} catch (error) {
	// 		console.log("error getSOLBalance => ", error);
	// 	}
	// }

	const saveUserCryptoWallets = async (user) => {
		let payload = {
			sup_user_id: user.id,
			wallet_address: publicKey.toBase58(),
		};
		var config = {
			method: "post",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/save_user_crypto_wallets`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + user.remember_token,
			},
			data: payload,
		};
		axios(config)
			.then(function (response) {})
			.catch(function (error) {
				console.log("saveUserCryptoWallets", error);
			});
	};

	async function getBETCBalance() {
		try {
			setIsLoading(true);
			const mint = new PublicKey(walletReducer.tokenMintAddress);

			const tokenAccountAddress = await getOrCreateAssociatedTokenAccount(
				connection,
				opSportWallet,
				mint,
				publicKey
			);

			let tokenAccountBalance = await connection.getTokenAccountBalance(
				tokenAccountAddress.address
			);
			let BETCbalance = tokenAccountBalance.value.uiAmount;
			dispatch({
				type: types.UPDATE_WALLET_BALANCE,
				payload: BETCbalance,
			});
			setIsLoading(false);
		} catch (error) {
			console.log("error getBETCBalance => ", error);
		}
	}

	return (
		<div className="mt-3">
			<p className="my-wallet-records">
				Balance:{" "}
				{isLoading
					? null
					: roundToDecimal(walletReducer?.walletBalance)}{" "}
				BETC
			</p>
		</div>
	);
}

export default WalletBalance;
