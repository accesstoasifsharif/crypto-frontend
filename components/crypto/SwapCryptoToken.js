import { useConnection, useWallet } from "@solana/wallet-adapter-react";

import {
	Keypair,
	PublicKey,
	SystemProgram,
	Transaction,
	TransactionSignature,
	LAMPORTS_PER_SOL,
} from "@solana/web3.js";

import { transfer, createTransferCheckedInstruction } from "@solana/spl-token";
import * as types from "../../store/actions/types";
import axios from "axios";

import { Modal, Button, Tabs, Tab } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import {
	getTokenAccountAddress,
	getSwapFeeAmount,
} from "../../src/cryptoHelper";
import { useEffect, useState } from "react";

function SwapCryptoToken(props) {
	const { connection } = useConnection();
	const { publicKey, sendTransaction, connected } = useWallet();
	const [showSwapTokenModal, setShowSwapTokenModal] = useState(false);
	const [showBuyForm, setShowBuyForm] = useState(true);
	const [showSellForm, setShowSellForm] = useState(false);
	const [isBuying, setIsBuying] = useState(false);
	const [isSelling, setIsSelling] = useState(false);
	const walletReducer = useSelector((state) => state.wallet);
	const [solAmount, setSolAmount] = useState("");
	const [betcAmount, setBetcAmount] = useState("");

	const [exchangeRate, setExchangeRate] = useState(null);
	const [loadingExchangeRate, setLoadingExchangeRate] = useState(false);
	const opSportWallet = walletReducer.opSportWallet;
	const swapFeePercentage = walletReducer.swapFeePercentage;
	const [opUser, setOpUser] = useState(null);

	useEffect(() => {
		setOpUser(getUserFromLocalStorage());
	}, []);

	function getUserFromLocalStorage() {
		return JSON.parse(localStorage.getItem("op_user")) || [];
	}
	const handleSolAmountChange = (event) => {
		setSolAmount(event.target.value);
	};

	const getBETCBuyCalculatedAmount = () => {
		let amount =
			(parseFloat(solAmount).toFixed(4) -
				computeSwapFeeAmount(solAmount)) *
			exchangeRate;
		if (amount) {
			return parseFloat(amount).toFixed(4);
		} else {
			return 0;
		}
	};

	const handleBetcAmountChange = (event) => {
		setBetcAmount(event.target.value);
	};

	const getBETCSellCalculatedAmount = () => {
		let sol_amount = betcAmount / exchangeRate;
		let amount =
			parseFloat(sol_amount).toFixed(4) -
			computeSwapFeeAmount(sol_amount);

		if (amount) {
			return parseFloat(amount).toFixed(4);
		} else {
			return 0;
		}
	};

	function showBuyTokenForm() {
		setShowSellForm(false);
		setShowBuyForm(true);
		setBetcAmount("");
		setSolAmount("");
	}
	function showSellTokenForm() {
		setShowBuyForm(false);
		setShowSellForm(true);
		setBetcAmount("");
		setSolAmount("");
	}
	const dispatch = useDispatch();

	const handleBuyToken = async () => {
		try {
			let finalSolAmount = solAmount;
			setIsBuying(true);
			const mint = new PublicKey(walletReducer.tokenMintAddress);
			const SolTrnx = new Transaction().add(
				SystemProgram.transfer({
					fromPubkey: publicKey,
					toPubkey: opSportWallet.publicKey,
					lamports: Math.round(finalSolAmount * LAMPORTS_PER_SOL),
				})
			);
			const SolTrnxSignature = await sendTransaction(SolTrnx, connection);
			const confirmTrnx = await connection.confirmTransaction(
				SolTrnxSignature,
				"processed"
			);
			if (confirmTrnx && SolTrnxSignature) {
				let solTrnxPayload = {
					transaction_id: SolTrnxSignature,
					transaction_type: 2,
					sending_wallet_address: publicKey.toBase58(),
					receiving_wallet_address:
						opSportWallet.publicKey.toBase58(),
					amount: finalSolAmount + "",
					currency: walletReducer.SOLCurrencyID,
				};
				saveCryptoTransactionLog(solTrnxPayload);
				const fromTokenAccount = await getTokenAccountAddress(
					connection,
					opSportWallet,
					mint,
					opSportWallet.publicKey
				);
				const toTokenAccount = await getTokenAccountAddress(
					connection,
					opSportWallet,
					mint,
					publicKey
				);
				await transfer(
					connection,
					opSportWallet,
					fromTokenAccount.address,
					toTokenAccount.address,
					opSportWallet.publicKey,
					Math.round(getBETCBuyCalculatedAmount() * LAMPORTS_PER_SOL),
					[]
				)
					.then((response) => {
						let tokenTrnxPayload = {
							transaction_id: response,
							transaction_type: 2,
							sending_wallet_address:
								opSportWallet.publicKey.toBase58(),
							receiving_wallet_address: publicKey.toBase58(),
							amount: getBETCBuyCalculatedAmount() + "",
							currency: walletReducer.BETCCurrencyID,
							reference_id: SolTrnxSignature,
						};
						saveCryptoTransactionLog(tokenTrnxPayload);

						let swapPayload = {
							sup_user_id: opUser.id,
							amount_from: finalSolAmount + "",
							amount_to: getBETCBuyCalculatedAmount() + "",
							currency_from: walletReducer.SOLCurrencyID,
							currency_to: walletReducer.BETCCurrencyID,
							exchange_rate: exchangeRate,
						};

						saveCryptoSwapLogs(swapPayload);

						updateWalletBalance(
							parseFloat(walletReducer.walletBalance) +
								parseFloat(getBETCBuyCalculatedAmount())
						);
						setIsBuying(false);
						closeSwapTokenModal();
					})
					.catch((error) => {
						console.log("tokenTrnx => ", error);
						setIsBuying(false);
					});
			}
		} catch (error) {
			console.log(error);
			setIsBuying(false);
		}
	};

	const saveCryptoTransactionLog = async (payload) => {
		var config = {
			method: "post",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/save_crypto_transaction`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + opUser.remember_token,
			},
			data: payload,
		};
		axios(config)
			.then(function (response) {})
			.catch(function (error) {
				console.log("saveCryptoTransactionLog", error);
			});
	};

	const saveCryptoSwapLogs = async (payload) => {
		var config = {
			method: "post",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/save_crypto_swap_logs`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + opUser.remember_token,
			},
			data: payload,
		};
		axios(config)
			.then(function (response) {})
			.catch(function (error) {
				console.log("saveCryptoTransactionLog", error);
			});
	};

	const handleSellToken = async () => {
		try {
			let finalSolAmount = getBETCSellCalculatedAmount();
			setIsSelling(true);

			const mint = new PublicKey(walletReducer.tokenMintAddress);
			const fromTokenAccount = await getTokenAccountAddress(
				connection,
				publicKey,
				mint,
				publicKey
			);
			const toTokenAccount = await getTokenAccountAddress(
				connection,
				publicKey,
				mint,
				opSportWallet.publicKey
			);

			let tokenTransaction = new Transaction().add(
				createTransferCheckedInstruction(
					fromTokenAccount.address, // from (should be a token account)
					mint, // mint
					toTokenAccount.address, // to (should be a token account)
					publicKey, // from's owner
					Math.round(betcAmount * LAMPORTS_PER_SOL), // amount,
					9
				)
			);

			const tokenTrnxSignature = await sendTransaction(
				tokenTransaction,
				connection
			);
			const confirmTrnx = await connection.confirmTransaction(
				tokenTrnxSignature,
				"processed"
			);

			if (confirmTrnx && tokenTrnxSignature) {
				let tokenTrnxPayload = {
					transaction_id: tokenTrnxSignature,
					transaction_type: 1,
					sending_wallet_address: publicKey.toBase58(),
					receiving_wallet_address:
						opSportWallet.publicKey.toBase58(),
					amount: betcAmount + "",
					currency: walletReducer.BETCCurrencyID,
				};
				saveCryptoTransactionLog(tokenTrnxPayload);

				let SolTrnx = new Transaction().add(
					SystemProgram.transfer({
						fromPubkey: opSportWallet.publicKey,
						toPubkey: publicKey,
						lamports: Math.round(finalSolAmount * LAMPORTS_PER_SOL),
					})
				);
				await connection
					.sendTransaction(SolTrnx, [opSportWallet])
					.then((response) => {
						let solTrnxPayload = {
							transaction_id: response,
							transaction_type: 1,
							sending_wallet_address:
								opSportWallet.publicKey.toBase58(),
							receiving_wallet_address: publicKey.toBase58(),
							amount: finalSolAmount + "",
							currency: walletReducer.BETCCurrencyID,
							reference_id: tokenTrnxSignature,
						};
						saveCryptoTransactionLog(solTrnxPayload);

						let swapPayload = {
							sup_user_id: opUser.id,
							amount_from: betcAmount + "",
							amount_to: finalSolAmount + "",
							currency_from: walletReducer.BETCCurrencyID,
							currency_to: walletReducer.SOLCurrencyID,
							exchange_rate: exchangeRate,
						};

						saveCryptoSwapLogs(swapPayload);
						updateWalletBalance(
							walletReducer.walletBalance - betcAmount
						);
						setIsSelling(false);
						closeSwapTokenModal();
					})
					.catch((error) => {
						console.log("tokenTrnx => ", error);
						setIsSelling(false);
					});
			}
		} catch (error) {
			console.log(error);
			setIsSelling(false);
		}
	};

	const updateWalletBalance = (amount = 0) => {
		dispatch({
			type: types.UPDATE_WALLET_BALANCE,
			payload: amount,
		});
	};

	// let solanaSolCurrentPriceInterval;
	const openSwapTokenModal = async () => {
		getSolanaSolCurrentPrice();
		// solanaSolCurrentPriceInterval = setInterval(function () {
		// 	getSolanaSolCurrentPrice();
		// }, 300000);
		setShowSwapTokenModal(true);
		showBuyTokenForm();
	};

	const getSolanaSolCurrentPrice = async () => {
		setLoadingExchangeRate(true);
		var url =
			"https://api.coingecko.com/api/v3/simple/price?ids=solana&vs_currencies=usd";
		await axios
			.get(url)
			.then(function (response) {
				setExchangeRate(response.data.solana.usd);
				setLoadingExchangeRate(false);
			})
			.catch(function (error) {
				console.log("getSolanaCurrentPrice", error);
			});
		return 1;
	};

	const closeSwapTokenModal = () => {
		// clearInterval(solanaSolCurrentPriceInterval);
		setShowSwapTokenModal(false);
	};

	const computeSwapFeeAmount = (sol_amount) => {
		if (sol_amount == 0) {
			return 0;
		}
		let calculatedAmount = (
			(parseFloat(sol_amount) / 100) *
			swapFeePercentage
		).toFixed(9);
		let amount = parseFloat(calculatedAmount).toFixed(4);
		return amount;
	};

	return (
		<>
			<Modal
				show={showSwapTokenModal}
				onHide={() => closeSwapTokenModal()}
				backdrop="static"
			>
				<Modal.Header closeButton>
					<h1 className="modal-title">BetCoin $BETC</h1>
				</Modal.Header>
				<Modal.Body>
					<div className="modal-header d-flex flex-column border-bottom-0">
						<p className="mb-0">
							Make sure you have enough SOL and then choose the
							amount of $BETC.
						</p>
					</div>

					<div className="d-flex">
						<button
							className={[
								"buy-sell-button ",
								showBuyForm ? "active-button" : "",
							].join(" ")}
							size="lg"
							onClick={() => showBuyTokenForm()}
						>
							Buy
						</button>
						<button
							className={[
								"buy-sell-button ",
								showSellForm ? "active-button" : "",
							].join(" ")}
							size="lg"
							onClick={() => showSellTokenForm()}
						>
							Sell
						</button>
					</div>

					{showBuyForm ? (
						<div className="px-3 py-4">
							<div className="form-group">
								<label>You Pay</label>
								<div className="modal-input-field">
									<span className="modal-input-field-span">
										SOL
									</span>
									<input
										disabled={
											exchangeRate == null ||
											loadingExchangeRate
										}
										type="number"
										className="form-control text-right"
										id="pay"
										value={solAmount}
										onChange={handleSolAmountChange}
									/>
								</div>
							</div>
							<table className="w-100">
								<tr>
									<th>Transaction Type</th>
									<th className="text-right">Amout</th>
								</tr>

								<tr>
									<td>Fee =</td>
									<td className="text-right">
										{computeSwapFeeAmount(solAmount)} SOL
									</td>
								</tr>
								<tr>
									<td>Total =</td>
									<td className="text-right">
										<b>
											{getBETCBuyCalculatedAmount()} BETC{" "}
										</b>
									</td>
								</tr>
								<tr>
									<td>
										<hr />
									</td>
									<td>
										<hr />
									</td>
								</tr>
							</table>
							<div className="d-flex justify-content-center pt-2">
								<button
									className="btn btn-primary rounded-pill"
									disabled={
										isBuying ||
										solAmount == "" ||
										solAmount == 0
									}
									onClick={() => handleBuyToken()}
								>
									{isBuying ? "Processing..." : "Buy BETC"}
								</button>
							</div>
						</div>
					) : null}
					{showSellForm ? (
						<div className="px-3 py-4">
							<div className="form-group">
								<label>You Pay</label>
								<div className="modal-input-field">
									<span className="modal-input-field-span">
										BETC
									</span>
									<input
										disabled={
											exchangeRate == null ||
											loadingExchangeRate
										}
										type="number"
										className="form-control text-right"
										id="pay"
										value={betcAmount}
										onChange={handleBetcAmountChange}
									/>
								</div>
							</div>

							<table className="w-100">
								<tr>
									<th>Transaction Type</th>
									<th className="text-right">Amout</th>
								</tr>
								<tr>
									<td>Fee =</td>
									<td className="text-right">
										{computeSwapFeeAmount(
											betcAmount / exchangeRate
										)}{" "}
										SOL
									</td>
								</tr>
								<tr>
									<td>Total =</td>
									<td className="text-right">
										<b>
											{getBETCSellCalculatedAmount()} SOL
										</b>
									</td>
								</tr>
								<tr>
									<td>
										<hr />
									</td>
									<td>
										<hr />
									</td>
								</tr>
							</table>
							<div className="d-flex justify-content-center pt-2">
								<button
									className="btn btn-primary rounded-pill"
									disabled={
										isSelling ||
										betcAmount == "" ||
										betcAmount == 0
									}
									onClick={() => handleSellToken()}
								>
									{isSelling ? "Processing..." : "Sell BETC"}
								</button>
							</div>
						</div>
					) : null}
					<div className="modal-footer d-flex justify-content-center border-top-0">
						<small>
							<strong>
								1 SOL ={" "}
								{loadingExchangeRate ? (
									<span>
										<i className="fa fa-spinner fa-spin"></i>{" "}
									</span>
								) : (
									exchangeRate
								)}{" "}
								BETC
							</strong>
						</small>
					</div>
				</Modal.Body>
			</Modal>

			{connected ? (
				<Button
					disabled={!publicKey}
					className="swap-token-button"
					onClick={() => openSwapTokenModal()}
				>
					Swap Token
				</Button>
			) : null}
		</>
	);
}

export default SwapCryptoToken;
