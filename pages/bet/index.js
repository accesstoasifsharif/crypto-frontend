import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from 'next/link';

import Footer from '../../components/layout/Footer';
import Header from '../../components/layout/Header';
import Navbar from '../../components/layout/Navbar';

import axios from "axios";
import rateLimit from 'axios-rate-limit';
import $ from 'jquery';
import Swal from "sweetalert2";
import UserAuth from '../../user_auth';
let UserCheckAuthObj = new UserAuth();


 function my_bet() {
    const router = useRouter();
    const [op_user, set_op_user] = useState(null);
    const [bets_started, set_bets_started] = useState(null);
    const [bets_started_p, set_bets_started_p] = useState([]);
    const [admin_cms_by_page_bet, set_admin_cms_by_page_bet] = useState(null);

    useEffect(() => {
        if(!router.isReady) return;
        UserCheckAuthObj.UserCheckAuth({router});
        var user_from_local_storage = getUserFromLocalStorage();
        set_op_user(user_from_local_storage);

        const myResponse = [];
        if(Object.keys(user_from_local_storage).length !== 0 && user_from_local_storage.constructor === Object){
            retrieveBetData(user_from_local_storage.id, user_from_local_storage.remember_token, "Bet")
            .then(
            axios.spread((...response) => {
                for (let i = 0; i <= 1; i += 1) {
                    myResponse.push(response[i].data)
                }
                //console.log(response)
                
                //if(myResponse[1].results.length != 0) set_bets_started(myResponse[1].results);
                if(myResponse[0].results.length != 0){
                    set_bets_started(myResponse[0].results);
                    set_bets_started_p(myResponse[0].results);
                    const data = { "req_per_page": document.getElementById("bets_started_req_per_page").value, "page_no": 1 };
                    bets_started_pagination(data, myResponse[0].results);
                } 

                if(myResponse[1].results.length != 0) set_admin_cms_by_page_bet(myResponse[1].results);
                
               
               
            })
            )
            .finally(() => {
                // Update refreshToken after 3 seconds so this event will re-trigger and update the data
                //setTimeout(() => setRefreshToken(Math.random()), 15000);
            });
        }
        
    }, [router.isReady]);
    
    function getUserFromLocalStorage(){
        return JSON.parse(localStorage.getItem('op_user')) || [];
    }
    const jsonBetsArrayFilterByTimeline = (arr, timeline)=>{
        return  arr.filter(function(arr_v){
             return arr_v.timeline == timeline;         
         })
     }
    async function retrieveBetData(user_id, remember_token, page) {
        const http = rateLimit(
            axios.create({headers: { 
                'Accept': 'application/json', 
                'Authorization': 'Bearer '+remember_token
            }
            }),
            {maxRequests: 4, perMilliseconds: 1000, maxRPS: 4}
        );
        const res = await axios.all([
            //http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=0`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=0,1`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/admin_cms_by_page?page=${page}`)/*,
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/games?timeline=0`) */
        ])
        
        return res;
    }
    const paginator = (items, current_page, per_page_items) => {
        //function paginator(items, current_page, per_page_items) {
            //console.log([typeof current_page, typeof per_page_items]);
            let page = current_page || 1,
            per_page = per_page_items || 10,
            offset = (page - 1) * per_page,
        
            paginatedItems = items.slice(offset).slice(0, per_page_items),
            total_pages = Math.ceil(items.length / per_page);
        
            return {
                page: page,
                per_page: per_page,
                pre_page: page - 1 ? page - 1 : null,
                next_page: (total_pages > page) ? page + 1 : null,
                total: items.length,
                total_pages: total_pages,
                data: paginatedItems
            };
        }
        // Pagination logic implementation
    const bets_started_pagination = (data, all_data) => {
        
        $("#bets_started_pagination").empty();
        if (data.req_per_page !== 'ALL') {
            const myarr = paginator(all_data, data.page_no, parseInt(data.req_per_page));
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            let pager = `<a id="prev_link" class="bets_started_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
                `<a class="bets_started_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;
            
            for (let num = 2; num <= myarr.total_pages; num++) {
                pager += `<a  class="bets_started_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
            }
            pager += `<a id="next_link"  class="bets_started_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;
            
            $("#bets_started_pagination").append(pager);
            
            $('div#bets_started_pagination a').each(function(){
                if(this.innerHTML == myarr.page){
                    $(this).addClass('active');
                }
            });

            bets_started_pagination_initialize('bets_started_p_btn','bets_started_req_per_page');
            set_bets_started(myarr.data);
            
        } else {
            const myarr = paginator(all_data, data.page_no, all_data.length);
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            set_bets_started(myarr.data);
        }
        
    }
    const bets_started_pagination_initialize = (pagination_btn,req_per_page_select) => {
        document.querySelectorAll('.'+pagination_btn).forEach(btn => {
            btn.addEventListener('click', () => {
                const page_no = btn.getAttribute("data-id");
                if(page_no !== 'null'){
                    const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": parseInt(page_no) };
                    bets_started_pagination(data, JSON.parse(btn.getAttribute("data-value")));
                }
                
            });
        });
    }
    const bets_started_filter_requests = async (bets_started_p,req_per_page_select) => {
        const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": 1 };
        //console.log([bets_completed_p, req_per_page_select]);
        bets_started_pagination(data, bets_started_p);
    }
    const bet_now = async (bets_started_row_v) => {
        console.log(bets_started_row_v);
        $("#bets_started_team1_value").text(bets_started_row_v.team1detail.name);
        $("#bets_started_team2_value").text(bets_started_row_v.team2detail.name);
        $("#bets_started_betteam_value").text(bets_started_row_v.betteamdetail.name);
        $("#bets_started_bet_amount_value").text('$'+bets_started_row_v.bet_amount);
        $("#bets_started_venue_value").text(bets_started_row_v.games.venue);
        $('#betNow_modal').modal('toggle');
    }
    console.log(admin_cms_by_page_bet);
  return (
    <>
    <style global jsx>{`
    .text-center.pagination_no_record {
        color: #fff;
    }
    `}</style>
    <section className="my-bet-top-bg">
  
    <Header /* data={op_user} */ />


    <Navbar /* data={op_user} */ />




    <section>
        <div className="container-fluid ">
            <div className="container py-2">
                <div className="row py-3">
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="images/industrial.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12  icn-content">
                                <p className="top-banner-heading">
                                    INDUSTRY FIRST
                                </p>
                                <p className="top-banner-desc"> Worlds 1st Web 3.0 Sportsbook</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="images/alarm-on.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12 icn-content">
                                <p className="top-banner-heading">
                                    FASTEST PAYOUTS
                                </p>
                                <p className="top-banner-desc"> Winnings automatically airdropped to wallet with no fee</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="images/welcome.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12 icn-content">
                                <p className="top-banner-heading">
                                    BET WITH STABLE CRYPTO
                                </p>
                                <p className="top-banner-desc">Never worry about the volatility of crypto when beting</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container-fluid  ">
            <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">

                <div className="carousel-inner">
                    {admin_cms_by_page_bet && admin_cms_by_page_bet.map((admin_cms_by_page_bet_row_v, admin_cms_by_page_bet_row_i)=>(
                        <div key={admin_cms_by_page_bet_row_i} className={admin_cms_by_page_bet_row_i == 0 ? 'carousel-item active':'carousel-item'}>
                            <img src={`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_bet_row_v?.cms_image}`} className="d-block w-100 p-4 my-bet-carousel-img" alt="..."/>
                            <div className="" dangerouslySetInnerHTML={{ __html: admin_cms_by_page_bet_row_v.cms_desc }} />
                        </div>
                    ))}
                    
                    {/* <div className="carousel-item">
                        <img src={ (admin_cms_by_page_bet) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_bet[1]?.cms_image}`):('/images/banner.webp')} className="d-block w-100 p-4 my-bet-carousel-img" alt="..."/>
                        <div className="carousel-caption  my-bet-carousel-caption">
                            <button className="my-bet-carousel-caption-button my-4"><a href="/promotion">GET DEPOSIT</a></button>
                        </div>
                    </div> */}
                </div>
                <button className="carousel-control-prev my-bet-left-btn" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
                    <i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
   
                </button>
                <button className="carousel-control-next my-bet-right-btn" type="button" data-target="#carouselExampleCaptions" data-slide="next">
                    <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
                </button>
            </div>
        </div>

        <div className="container-fluid">
            <div className="container">
                <div className="row">
                    <div className="col-12 my-current-bets my-5">
                        <h3>Current Live Bets</h3>
                    </div>
                </div>
                {bets_started && bets_started.map((bets_started_row_v, bets_started_row_i)=>(
                <div key={bets_started_row_i} className="card mx-auto  my-live-bet-matches my-5">
                    <div className="card-body">
                        <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <h5 className=""><button className="live-button"></button>Live {bets_started_row_i+1}</h5>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">SPREAD</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                            </div>
                        </div>
                        <div className="row card-title-list">
                            <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                <p className="my-live-bet-matches-list">{bets_started_row_v.team1detail.name} vs {bets_started_row_v.team2detail.name}</p>
                            </div>
                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                <p className="my-live-bet-matches-list">{bets_started_row_v.team1detail.name}</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2" style={{background:bets_started_row_v.bet_box == 1 ? '#008000':'#0EBDD5' }}>
                                    <span>{bets_started_row_v.spread1_t1}</span>
                                    <span>{bets_started_row_v.spread2_t1}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2" style={{background:bets_started_row_v.bet_box == 2 ? '#008000':'#0EBDD5' }}>
                                <span>{bets_started_row_v.moneyline_t1}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2" style={{background:bets_started_row_v.bet_box == 3 ? '#008000':'#0EBDD5' }}> <span>{bets_started_row_v.total1_type_t1+ ' ' + bets_started_row_v.total1_t1}</span> 
                                <span>{bets_started_row_v.total2_t1}</span></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <p className="my-live-bet-matches-list">{bets_started_row_v.team2detail.name}</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2" style={{background:bets_started_row_v.bet_box == 4 ? '#008000':'#0EBDD5' }}>
                                    <span>{bets_started_row_v.spread1_t2}</span>
                                    <span>{bets_started_row_v.spread2_t2}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2" style={{background:bets_started_row_v.bet_box == 5 ? '#008000':'#0EBDD5' }}>
                                <span>{bets_started_row_v.moneyline_t2}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2" style={{background:bets_started_row_v.bet_box == 6 ? '#008000':'#0EBDD5' }}> <span>{bets_started_row_v.total1_type_t2+ ' ' + bets_started_row_v.total1_t2}</span> 
                                <span>{bets_started_row_v.total2_t2}</span></button>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <a className="match-your-teams"  data-toggle="tooltip" title="Get Preep Details" onClick={()=>bet_now(bets_started_row_v)}>Get Preep</a>
                        </div>
                    </div>
                </div>
                ))}
                {/* <div className="card mx-auto  my-live-bet-matches my-5">
                    <div className="card-body">
                        <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <h5 className=""><button className="live-button"></button>Live 3rd</h5>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">SPREAD</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                            </div>
                        </div>
                        <div className="row card-title-list">
                            <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                <p className="my-live-bet-matches-list">Fc Barcelona vs Real Madrid</p>
                            </div>
                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                <p className="my-live-bet-matches-list">Fc Barcelona</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <p className="my-live-bet-matches-list">Real Madrid</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <a href="#" className="match-your-teams">Get Preep</a>
                        </div>
                    </div>
                </div>
                <div className="card mx-auto  my-live-bet-matches my-5">
                    <div className="card-body">
                        <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <h5 className=""><button className="live-button"></button>Live 3rd</h5>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">SPREAD</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                            </div>
                        </div>
                        <div className="row card-title-list">
                            <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                <p className="my-live-bet-matches-list">Fc Barcelona vs Real Madrid</p>
                            </div>
                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                <p className="my-live-bet-matches-list">Fc Barcelona</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <p className="my-live-bet-matches-list">Real Madrid</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <a href="" className="match-your-teams">Get Preep</a>
                        </div>
                    </div>
                </div> */}

                        <div className="paginator_btn">
                            <div className="clearfix">
                                <div className="box options">
                                    <label>Requests Per Page: </label>
                                    <select id="bets_started_req_per_page" onChange={() => bets_started_filter_requests(bets_started_p,'bets_started_req_per_page')}>
                                        <option>5</option>
                                        <option>1</option>
                                        <option>10</option>
                                        <option>ALL</option>
                                    </select>
                                </div>
                                <div id="bets_started_pagination" className="box pagination">
                                </div>
                            </div>
                        </div>

                        {bets_started==null ? (<div className="text-center pagination_no_record">No Record</div>):('')}
            </div>
        </div>
        <div className="container-fluid my-bet-overlay-bg">
            <div className="container py-5">
                <div className="row">
                    <div className="col-12  my-5">
                        <h3 className="my-current-problems">
                            <a href="https://www.ncpgambling.org/help-treatment/national-helpline-1-800-522-4700/" target="_blank" className="text-white">
                                If you have a Gambling Problem?
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>

        {/* <!-- betting model --> */}
        <div className="modal fade betNowModal" id="betNow_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Bet Detail</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            {/* <form onSubmit={bet_now_submit}> */}
                            <div className="modal-body">
                                    {/* <div className="row">
                                        <div className="col-md-6">
                                                Id
                                        </div>
                                        <div className="col-md-6">
                                                Bet on team
                                        </div>
                                    </div> */}
                                    <div className="row">
                                        <div className="col-md-6">Team1</div>
                                        <div className="col-md-6 ml-auto" id="bets_started_team1_value"></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">Team2</div>
                                        <div className="col-md-6 ml-auto" id="bets_started_team2_value"></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">Your Team</div>
                                        <div className="col-md-6 ml-auto" id="bets_started_betteam_value"></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">Amount</div>
                                        <div className="col-md-6 ml-auto" id="bets_started_bet_amount_value"></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">Venue</div>
                                        <div className="col-md-6 ml-auto" id="bets_started_venue_value"></div>
                                    </div>
                                    
                            </div>
                            <div className="modal-footer">
                                {/* <button type="button" className="placebet_btn">Place Bet</button> */}
                                <button type="button" className="close placebet_btn" data-dismiss="modal" aria-label="Close">
                                    Ok
                                </button>
                            </div>
                            {/* </form> */}
                        </div>
                    </div>
                </div>
        {/* <!-- //betting model --> */}
    </section>
    <Footer/>
   
</section>

    
    
    
    
    
    
    </>
    )
}
export default my_bet;