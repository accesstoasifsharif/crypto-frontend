import React from 'react'


function bet_popup() {
  return (
    <>
   

  <section>
      <div className="container mybet-popup-content">
        <div className="row  mybet-popup-content-inner-cintent">
              <div className="col-md-4 text-lg-left text-center col-12">
                <button type="button" className="btn popup-btn">OPSportsbook</button>
  </div> 
            <div className="col-md-4 col-6 mx-auto">
                <div className="form-group has-search">
                    <span className="fa fa-search form-control-feedback"></span>
                    <input type="text" className="form-control" placeholder="Search"/>
                </div>
            </div>
            <div className="col-md-4"></div>
        </div>
        <div className="row py-3 mybet-popup-content-inner-cintent">
            <div className="col-md-4 d-flex p-0 justify-content-center">
                <h3 className="Bet-Slip">Bet Slip(1)</h3>
                <img src="images/arrow-down-sign-to-navigate.svg" alt="" className="arrow-down-sign-to-navigate"/>
            </div>
            <div className="col-md-4"></div>
            <div className="col-md-4 justify-content-center d-flex">
                <h3 className="Open-Bets">Open Bets</h3>
                <img src="images/arrow-up-sign-to-navigate.svg" alt="" className="arrow-down-sign-to-navigate"/>
            </div>
        </div>
        <div className="row popup-items-menu">
            <div className="col-md-12 px-0">
                <ul className="  py-3 m-0 popup-items-menu-list ">
                    <li className="px-4 popup-items"><a href="" className=" popup-items-links">Straight</a></li>
                    <li className="px-4 popup-items"><a href="" className=" popup-items-links">Parley</a></li>
                    <li className="px-4 popup-items"><a href="" className=" popup-items-links">Teaser</a></li>
                    <li className="px-4 popup-items"><a href="" className=" popup-items-links">Odds</a></li>
                    <li className="px-4 popup-items"><a href="" className=" popup-items-links">RR</a></li>
                </ul>
            </div>
        </div>
        <div className="row mybet-popup-body-content">
            <div className="col-md-12 mt-5 text-right">
                <a href="" className="Clear-Bet-Slip">Clear Bet Slip</a>
            </div>
            <div className="col-md-12">
                <div className="card w-50 mx-auto mybet-popup">
                    <div className="row mybet-popup-card py-3">

                        <div className="col-md-6 col-12 text-lg-left text-center">
                            <h4 className="bet-team">FC Barcelona</h4>
                        </div>
                        <div className="col-md-6 text-right col-12 text-lg-right text-center">
                            <h4 className="bet-unit">-45</h4>
                        </div>
                        <div className="col-md-12 text-left">
                            <h6 className="bet-matches">
                                Spread FC Barcelona Vs Real Madrid (LIVE)
                            </h6>
                        </div>
                        <div className="col-md-12 text-lg-right py-2 text-center">
                            <h6 className="bonus-units ">Win 0.00</h6>
                        </div>
                        <div className="col-md-6 text-lg-left text-center ">
                            <div className="dropdown">
                                <button className="btn  dropdown-toggle Buy-Points" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                    Buy Points
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a className="dropdown-item" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 text-lg-left text-center">
                            <div className="dropdown">
                                <button className="btn  dropdown-toggle Buy-Points" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                    Bet $
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a className="dropdown-item" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


          <div className="col-md-4 offset-md-3 pt-4 text-lg-left text-center">
                <p className="total-bets font_set">Total Bets </p>
            </div>
            <div className="col-md-2 offset-md-3-end pt-4 text-lg-left text-center col-12">
                <p className="bet-amounts font_set2 text-center text-lg-right"> 0</p>
            </div>
            <div className="col-md-4 offset-md-3 col-12 text-center text-lg-left">
                <p className="total-bets font_set"> Total Bet</p>
            </div>
            <div className="col-md-2 offset-md-3-end  ">
                <p className="bet-amounts font_set2 text-lg-right text-center"> $0.00</p>
            </div>
            <div className="col-md-4 offset-md-3 text-lg-left text-center">
                <p className="total-bets font_set"> Amount Potential Win Amount</p>
            </div>
            <div className="col-md-2 offset-md-3-end text-lg-right text-center">
                <p className="bet-amounts font_set2 text-lg-right text-center"> $0.00 </p>
            </div>

            <div className="col-md-12 text-center my-5">
                <a href="" className="DEPOSIT-NOW">DEPOSIT NOW YO PLACE BET</a>
            </div>

        </div>

    </div>

    </section> 

</>
  )
}
export default bet_popup;