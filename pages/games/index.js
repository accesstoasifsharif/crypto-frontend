import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from 'next/link';

import Footer from '../../components/layout/Footer';
import Header from '../../components/layout/Header';
import Navbar from '../../components/layout/Navbar';

import axios from "axios";
import rateLimit from 'axios-rate-limit';
import $ from 'jquery';
import Swal from "sweetalert2";
import NProgress from 'nprogress';
import moment from 'moment';
import UserAuth from '../../user_auth';
let UserCheckAuthObj = new UserAuth();


 function games() {
    const router = useRouter();
    const [op_user, set_op_user] = useState(null);
    const [bets_started, set_bets_started] = useState(null);
    const [bets_started_p, set_bets_started_p] = useState([]);
    const [games_upcoming, set_games_upcoming] = useState(null);
    const [games_upcoming_p, set_games_upcoming_p] = useState([]);
    const [admin_cms_by_page_bet, set_admin_cms_by_page_bet] = useState(null);
    const [user_search, set_user_search] = useState('');
    const [user_search_c, set_user_search_c] = useState('');
    
    if(router.query.hasOwnProperty("search")){
        setTimeout(()=>{
            var { search } = router.query;
            set_user_search_c(user_search);
            set_user_search(search);
            //console.log(router.query);
        },100);
    } 
    useEffect(() => {
        if(!router.isReady) return;
        // UserCheckAuthObj.UserCheckAuth({router});
        var user_from_local_storage = getUserFromLocalStorage();
        set_op_user(user_from_local_storage);

        const myResponse = [];
        //if(Object.keys(user_from_local_storage).length !== 0 && user_from_local_storage.constructor === Object){
            retrieveBetData(/* user_from_local_storage.id, user_from_local_storage.remember_token,  */"Bet", user_search)
            .then(
            axios.spread((...response) => {
                for (let i = 0; i <= 1; i += 1) {
                    myResponse.push(response[i].data)
                }
                //console.log(response)
                
                //if(myResponse[1].results.length != 0) set_bets_started(myResponse[1].results);
                if(myResponse[0].results.length != 0){
                    set_games_upcoming(myResponse[0].results);
                    set_games_upcoming_p(myResponse[0].results);
                    const data = { "req_per_page": document.getElementById("req_per_page").value, "page_no": 1 };
                    pagination(data, myResponse[0].results);
                } 

                if(myResponse[1].results.length != 0) set_admin_cms_by_page_bet(myResponse[1].results);
                
                // localStorage.setItem("op_user",JSON.stringify(myResponse[2].user));
                // set_op_user(myResponse[2].user);
                
               
               
            })
            )
            .finally(() => {
                // Update refreshToken after 3 seconds so this event will re-trigger and update the data
                //setTimeout(() => setRefreshToken(Math.random()), 15000);
            });
        //}
        
    }, [router.isReady, user_search]);
    
    function getUserFromLocalStorage(){
        return JSON.parse(localStorage.getItem('op_user')) || [];
    }
    const jsonBetsArrayFilterByTimeline = (arr, timeline)=>{
        return  arr.filter(function(arr_v){
             return arr_v.timeline == timeline;         
         })
     }
    async function retrieveBetData(/* user_id, remember_token,  */page, user_search) {
        const http = rateLimit(
            axios.create({headers: { 
                'Accept': 'application/json'/* , 
                'Authorization': 'Bearer '+remember_token */
            }
            }),
            {maxRequests: 4, perMilliseconds: 1000, maxRPS: 4}
        );
        NProgress.start();
        const res = await axios.all([
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/games_s?timeline=0&search=${user_search}`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/admin_cms_by_page?page=${page}`)
        ])
        NProgress.done();
        return res;
    }
    const paginator = (items, current_page, per_page_items) => {
        //function paginator(items, current_page, per_page_items) {
            //console.log([typeof current_page, typeof per_page_items]);
            let page = current_page || 1,
            per_page = per_page_items || 10,
            offset = (page - 1) * per_page,
        
            paginatedItems = items.slice(offset).slice(0, per_page_items),
            total_pages = Math.ceil(items.length / per_page);
        
            return {
                page: page,
                per_page: per_page,
                pre_page: page - 1 ? page - 1 : null,
                next_page: (total_pages > page) ? page + 1 : null,
                total: items.length,
                total_pages: total_pages,
                data: paginatedItems
            };
    }
    
    // Pagination logic implementation
    const pagination = (data, all_data) => {
        
        $("#games_upcoming_pagination").empty();
        if (data.req_per_page !== 'ALL') {
            const myarr = paginator(all_data, data.page_no, parseInt(data.req_per_page));
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            let pager = `<a id="prev_link" class="games_upcoming_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
                `<a class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;
            
            for (let num = 2; num <= myarr.total_pages; num++) {
                pager += `<a  class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
            }
            pager += `<a id="next_link"  class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;
            
            $("#games_upcoming_pagination").append(pager);
            
            $('div#games_upcoming_pagination a').each(function(){
                if(this.innerHTML == myarr.page){
                    $(this).addClass('active');
                }
            });

            pagination_initialize('games_upcoming_p_btn','req_per_page');
            set_games_upcoming(myarr.data);
            
        } else {
            const myarr = paginator(all_data, data.page_no, all_data.length);
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            set_games_upcoming(myarr.data);
        }
        
    }

    // trigger when requests per page dropdown changes
    const pagination_initialize = (pagination_btn,req_per_page_select) => {
        document.querySelectorAll('.'+pagination_btn).forEach(btn => {
            btn.addEventListener('click', () => {
                const page_no = btn.getAttribute("data-id");
                if(page_no !== 'null'){
                    const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": parseInt(page_no) };
                    pagination(data, JSON.parse(btn.getAttribute("data-value")));
                }
                
            });
        });
    }
    const filter_requests = async (games_upcoming_p,req_per_page_select) => {
        const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": 1 };
        pagination(data, games_upcoming_p);
    }
    
    const AmericanToDecimal = (american_ods) => {
		var decimal_ods= 0;
		if(american_ods > 0){
			var convert = (1 + (american_ods/100));
		}else{
			var convert = (1 - (100/american_ods));
		}
		
		if(convert != 'Infinity' && convert != '-Infinity'){
			decimal_ods = convert;	
		}
        return decimal_ods;
	}
    const DecimalToAmerican = (decimalodd) => {
        var american_odds = 0;
		if(decimalodd >= 2){
			var convert = ((decimalodd-1) * 100);
		}else{
			var convert = ((-100)/(decimalodd-1));
		}
		//console.log(convert);
        
		if(convert != 'Infinity' && convert != '-Infinity'){
			//$('#american_ods').val(convert);	
            american_odds = convert;
		}else{	
		
		}
		return american_odds;
		if(decimalodd > 0 ){
			var fractional_ods = ((decimalodd -1 )/1);
		}else{
			var fractional_ods = (decimalodd -1 );
		}
		//$('#fractional_ods').val(dec_convert);
		//pay_out_odds();
	}

    const DecimalToFraction = (decimalodd) => {
        var fractional_ods = 0;
		
		if(decimalodd > 0 ){
			var convert = ((decimalodd -1 )/1);
		}else{
			var convert = (decimalodd -1 );
		}
		//$('#fractional_ods').val(dec_convert);
		//pay_out_odds();
        if(convert != 'Infinity' && convert != '-Infinity'){
			//$('#american_ods').val(convert);	
            fractional_ods = convert;
		}
            return fractional_ods;
	}


    const convertToImplideOdds = (betamount, decimal_ods) => {
		// var betamount=$('#bet_amount').val();
		// var decimal_ods=$('#decimal_ods').val();
        var convert_to_impliedodds = 0;
		var convert= ((betamount) / (decimal_ods ));
		//$('#implide_ods').val(convert_to_impliedodds);

        if(convert != 'Infinity' && convert != '-Infinity'){
			//$('#american_ods').val(convert);	
            convert_to_impliedodds = convert;
		}

        return convert_to_impliedodds;

	}
    const bet_now = async (upcoming_games_row_v, decimal_ods, team, bet_box) => {
        var res_ad = JSON.parse(localStorage.getItem('op_user')) || {};
        if(Object.keys(res_ad).length !== 0 && res_ad.constructor === Object){
            var data = new Object();
            var config = {
                method: 'post',
                url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile`,
                headers: { 
                    'Accept': 'application/json', 
                    'Authorization': 'Bearer '+res_ad.remember_token
                    //...data.getHeaders()
                },
                data : data
            };
    
            axios(config)
            .then(function (response) {
                if(response.data.hasOwnProperty('message')){
                    if(response.data.message == 'Authenticated'){
                        if(Object.keys(response.data.user).length !== 0 && response.data.user.constructor === Object){
                            var american_ods =  DecimalToAmerican(decimal_ods).toFixed(2);
                            var fractional_ods =  DecimalToFraction(decimal_ods).toFixed(2);
                            //console.log(decimal_ods);
                            // $('#bet_team').empty();
                            // $('#bet_team').append($('<option>').text(upcoming_games_row_v.team1detail.name).attr('value', 1));
                            // $('#bet_team').append($('<option>').text(upcoming_games_row_v.team2detail.name).attr('value', 2));
                            // $("#bet_team option[value='"+team+"']").prop("selected", true);
                            // if(team == 1) $("#bet_team option[value='2']").attr("disabled","disabled");
                            // else $("#bet_team option[value='1']").attr("disabled","disabled");
                            var teamname = '';
                            if(team == 1) teamname = upcoming_games_row_v.team1detail.name;
                            else teamname = upcoming_games_row_v.team2detail.name;
                            $("#bet_team").val(team);
                            $("#bet_team1").val(teamname);
                            $("#game_id").val(upcoming_games_row_v.id);
                            $("#american_ods").val(american_ods);
                            $("#decimal_ods").val(decimal_ods);
                            $("#fractional_ods").val(fractional_ods);
                            $("#bet_box").val(bet_box);
                            //$("#user_id").val(op_user.id);
                            $('#betNow_modal').modal('toggle');
                        }else{
                            router.push("/login");
                        }
                    }else{
                        router.push("/login");
                    }
                }else{
                    router.push("/login");
                }
            })
            .catch(function (error) {
                router.push("/login");
            });
        }else{
            router.push("/login");
        }
        
    }
    const bet_now_submit = async event => {
        event.preventDefault();
        //console.log([event.target.user_id.value, event.target.game_id.value,event.target.bet_amount.value,event.target.bet_team.value]);
        
        var alert_message = 'Please Fill These Fields: <br>';
        var newLine = "\r\n";
        alert_message += newLine;
        
      if(/* event.target.user_id.value=='' ||  */event.target.game_id.value=='' || event.target.bet_amount.value=='' || event.target.bet_team.value==''){
        
        /* if(event.target.user_id.value==''){
            alert_message += "User Id can not be left blank. <br>";
            alert_message += newLine;
        } */
        if(event.target.game_id.value==''){
          alert_message += "Game Id can not be left blank. <br>";
          alert_message += newLine;
        }
        if(event.target.bet_amount.value==''){
            alert_message += "Amount can not be left blank. <br>";
            alert_message += newLine;
        }
        if(event.target.bet_team.value==''){
            alert_message += "Team can not be left blank. <br>";
            alert_message += newLine;
        }
        
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          //text: alert_message
          html: alert_message
          //footer: '<a href="">Why do I have this issue?</a>'
        })
        return false;
    }
    
        var implide_ods =  convertToImplideOdds(event.target.bet_amount.value, event.target.decimal_ods.value).toFixed(2);
        var data = new Object();
        //data.user_id = event.target.user_id.value;
        data.game_id = event.target.game_id.value;
        data.bet_amount = event.target.bet_amount.value;
        data.bet_team = event.target.bet_team.value;
        data.american_ods = event.target.american_ods.value;
        data.decimal_ods = event.target.decimal_ods.value;
        data.fractional_ods = event.target.fractional_ods.value;
        data.implide_ods = implide_ods;
        data.bet_box = event.target.bet_box.value;
        var config = {
            method: 'post',
            url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets/add`,
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+op_user.remember_token
            },
            data : data
          };
          
          axios(config)
          .then(function (response) {
            if(response.data.hasOwnProperty('message')){
                if(response.data.message == 'Authenticated'){
                    //console.log(response.data);
                    if(response.data.message_title == 'success'){
                        // set_games_upcoming(response.data.results);
                        // set_games_upcoming_p(response.data.results);
                        // const data = { "req_per_page": document.getElementById("req_per_page").value, "page_no": 1 };
                        // pagination(data, response.data.results);
                        swalAutoclose('success', 'Bet Placed Successfully', 1000);
                    }else{
                        Swal.fire('Unable to bet '+response.data.results);
                    }
                    
                }else{
                    swalAutoclose('error', 'Unable to bet', 1000)
                }
                
            }else{
                swalAutoclose('error', 'Unable to bet', 1000)
            }
            
          })
          .catch(function (error) {
              swalAutoclose('error', 'Unable to bet', 1000)
          });
    }
    const swalAutoclose = (title, html, timer, timer_progress_bar=false)=>{
        let timerInterval
        return Swal.fire({
            title: title,
            html: html,
            timer: timer,
            timerProgressBar: timer_progress_bar,
            didOpen: () => {
                Swal.showLoading()
                //const b = Swal.getHtmlContainer().querySelector('b')
                // timerInterval = setInterval(() => {
                // b.textContent = Swal.getTimerLeft()
                // }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                //console.log('I was closed by the timer')
            }
        })
    }
    
    // if(user_search != user_search_c){
    //     console.log(user_search);
    // }
    //console.log(games_upcoming);
  return (
    <>
    <style global jsx>{`
    .text-center.pagination_no_record {
        color: #fff;
    }
    `}</style>
    <section className="my-bet-top-bg">
  
    <Header /* data={op_user} */ />


    <Navbar /* data={op_user} */ />




    <section>
        <div className="container-fluid ">
            <div className="container py-2">
                <div className="row py-3">
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="images/industrial.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12  icn-content">
                                <p className="top-banner-heading">
                                    INDUSTRY FIRST
                                </p>
                                <p className="top-banner-desc"> Worlds 1st Web 3.0 Sportsbook</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="images/alarm-on.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12 icn-content">
                                <p className="top-banner-heading">
                                    FASTEST PAYOUTS
                                </p>
                                <p className="top-banner-desc"> Winnings automatically airdropped to wallet with no fee</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="images/welcome.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12 icn-content">
                                <p className="top-banner-heading">
                                    BET WITH STABLE CRYPTO
                                </p>
                                <p className="top-banner-desc">Never worry about the volatility of crypto when beting</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container-fluid  ">
            <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">

                <div className="carousel-inner">
                    {admin_cms_by_page_bet && admin_cms_by_page_bet.map((admin_cms_by_page_bet_row_v, admin_cms_by_page_bet_row_i)=>(
                        <div key={admin_cms_by_page_bet_row_i} className={admin_cms_by_page_bet_row_i == 0 ? 'carousel-item active':'carousel-item'}>
                            <img src={`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_bet_row_v?.cms_image}`} className="d-block w-100 p-4 my-bet-carousel-img" alt="..."/>
                            <div className="" dangerouslySetInnerHTML={{ __html: admin_cms_by_page_bet_row_v.cms_desc }} />
                        </div>
                    ))}
                </div>
                <button className="carousel-control-prev my-bet-left-btn" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
                    <i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
   
                </button>
                <button className="carousel-control-next my-bet-right-btn" type="button" data-target="#carouselExampleCaptions" data-slide="next">
                    <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
                </button>
            </div>
        </div>

        <div className="container-fluid">
            {/* <div className="container">
                <div className="row">
                    <div className="col-12 my-current-bets my-5">
                        <h3>Current Live Bets</h3>
                    </div>
                </div>
                {bets_started && bets_started.map((bets_started_row_v, bets_started_row_i)=>(
                <div key={bets_started_row_i} className="card mx-auto  my-live-bet-matches my-5">
                    <div className="card-body">
                        <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <h5 className=""><button className="live-button"></button>Live {bets_started_row_i+1}</h5>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">SPREAD</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                            </div>
                        </div>
                        <div className="row card-title-list">
                            <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                <p className="my-live-bet-matches-list">{bets_started_row_v.team1detail.name} vs {bets_started_row_v.team2detail.name}</p>
                            </div>
                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                <p className="my-live-bet-matches-list">{bets_started_row_v.team1detail.name}</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2">
                                    <span>{bets_started_row_v.spread1_t1}</span>
                                    <span>{bets_started_row_v.spread2_t1}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2">
                                <span>{bets_started_row_v.moneyline_t1}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"> <span>{bets_started_row_v.total1_type_t1+ ' ' + bets_started_row_v.total1_t1}</span> 
                                <span>{bets_started_row_v.total2_t1}</span></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <p className="my-live-bet-matches-list">{bets_started_row_v.team2detail.name}</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2">
                                    <span>{bets_started_row_v.spread1_t2}</span>
                                    <span>{bets_started_row_v.spread2_t2}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2">
                                <span>{bets_started_row_v.moneyline_t2}</span>
                                </button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"> <span>{bets_started_row_v.total1_type_t2+ ' ' + bets_started_row_v.total1_t2}</span> 
                                <span>{bets_started_row_v.total2_t2}</span></button>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <a className="match-your-teams"  data-toggle="tooltip" title="Get Preep Details" onClick={()=>bet_now(bets_started_row_v)}>Get Preep</a>
                        </div>
                    </div>
                </div>
                ))}
                

                        <div className="paginator_btn">
                            <div className="clearfix">
                                <div className="box options">
                                    <label>Requests Per Page: </label>
                                    <select id="bets_started_req_per_page" onChange={() => bets_started_filter_requests(bets_started_p,'bets_started_req_per_page')}>
                                        <option>5</option>
                                        <option>1</option>
                                        <option>10</option>
                                        <option>ALL</option>
                                    </select>
                                </div>
                                <div id="bets_started_pagination" className="box pagination">
                                </div>
                            </div>
                        </div>

                        {bets_started==null ? (<div className="text-center pagination_no_record">No Record</div>):('')}
            </div> */}


            <div className="container">
            <div className="row">
                    <div className="col-md-12 text-center">
                        <p className="user-bank-roles-heading">Current Game List</p>
                    </div>
                </div>
                <div className="row py-5 game_list_row">
                {games_upcoming && games_upcoming.map((games_upcoming_row_v,games_upcoming_row_i) => (
                   

                    <div key={games_upcoming_row_i} className="card mx-auto  my-live-bet-matches my-5">
                        <div className="card-body">
                            <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                                <div className="col-md-4 col-sm-12 col-12 live-bet-matches">
                                    <h5 className=""><button className="live-button"></button>Live {games_upcoming_row_i+1}</h5>
                                </div>
                                <div className="col-md-2 d-flex align-items-center"><span className="my-live-bet-matches-tabs text-nowrap">{moment(games_upcoming_row_v?.game_date,"YYYY-MM-DD HH:mm:ss").format('YYYY-MM-DD') == moment().format("YYYY-MM-DD") ? 'Today':
                                                                                                        moment(games_upcoming_row_v?.game_date,"YYYY-MM-DD HH:mm:ss").format('YYYY-MM-DD') == moment().add(-1, 'days').format("YYYY-MM-DD") ? 'Yesterday':
                                                                                                        moment(games_upcoming_row_v?.game_date,"YYYY-MM-DD HH:mm:ss").format('d MMMM YYYY')} {moment(games_upcoming_row_v?.game_date,"YYYY-MM-DD HH:mm:ss").format('h:mm A')}</span></div>
                                <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                    <p className="my-live-bet-matches-tabs">SPREAD</p>
                                </div>
                                <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                    <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                                </div>
                                <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                    <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                                </div>
                            </div>
                            <div className="row card-title-list">
                                <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                    <p className="my-live-bet-matches-list">{games_upcoming_row_v.team1detail.name} vs {games_upcoming_row_v.team2detail.name}</p>
                                </div>
                            </div>
                            <div className="row mt-3 mb-3">
                                <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                    <p className="my-live-bet-matches-list">{games_upcoming_row_v.team1detail.name}</p>
                                </div>
                                <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                    <button type="button"  data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v, games_upcoming_row_v.spread2_t1.toFixed(2), 1, 1)} className="btn my-bet-team mt-2">
                                        <span>{games_upcoming_row_v.spread1_t1.toFixed(2)}</span>
                                        <span>{games_upcoming_row_v.spread2_t1.toFixed(2)}</span>
                                    </button>
                                </div>
                                <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                    <button type="button"  data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v, games_upcoming_row_v.moneyline_t1.toFixed(2), 1, 2)} className="btn my-bet-team mt-2">
                                    <span>{games_upcoming_row_v.moneyline_t1.toFixed(2)}</span>
                                    </button>
                                </div>
                                <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                    <button type="button"  data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v, games_upcoming_row_v.total2_t1.toFixed(2), 1, 3)} className="btn my-bet-team mt-2"> <span>{games_upcoming_row_v.total1_type_t1+ ' ' + games_upcoming_row_v.total1_t1.toFixed(2)}</span> 
                                    <span>{games_upcoming_row_v.total2_t1.toFixed(2)}</span></button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                    <p className="my-live-bet-matches-list">{games_upcoming_row_v.team2detail.name}</p>
                                </div>
                                <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                    <button type="button"  data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v, games_upcoming_row_v.spread2_t2.toFixed(2), 2, 4)} className="btn my-bet-team mt-2">
                                        <span>{games_upcoming_row_v.spread1_t2.toFixed(2)}</span>
                                        <span>{games_upcoming_row_v.spread2_t2.toFixed(2)}</span>
                                    </button>
                                </div>
                                <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                    <button type="button"  data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v, games_upcoming_row_v.moneyline_t2.toFixed(2), 2, 5)} className="btn my-bet-team mt-2">
                                    <span>{games_upcoming_row_v.moneyline_t2.toFixed(2)}</span>
                                    </button>
                                </div>
                                <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                    <button type="button"  data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v, games_upcoming_row_v.total2_t2.toFixed(2), 2, 6)} className="btn my-bet-team mt-2"> <span>{games_upcoming_row_v.total1_type_t2+ ' ' + games_upcoming_row_v.total1_t2.toFixed(2)}</span> 
                                    <span>{games_upcoming_row_v.total2_t2.toFixed(2)}</span></button>
                                </div>
                            </div>
                            <div className="col-md-12 text-right">
                                {/* <a className="match-your-teams"  data-toggle="tooltip" title="Get Preep Details" onClick={()=>bet_now(games_upcoming_row_v)}>Get Preep</a> */}
                                {/* <a className="betnow_btn" data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v)}>Bet Now</a> */}
                            </div>
                        </div>
                    </div>

                ))}

                    
                    <div className="paginator_btn">
                        <div className="clearfix">
                            <div className="box options">
                                <label>Requests Per Page: </label>
                                <select id="req_per_page" onChange={() => filter_requests(games_upcoming_p,'req_per_page')}>
                                    <option>5</option>
                                    <option>1</option>
                                    <option>10</option>
                                    <option>ALL</option>
                                </select>
                            </div>
                            <div id="games_upcoming_pagination" className="box pagination">
                            </div>
                        </div>
                    </div>

                    {games_upcoming == null ? (<div className="text-center pagination_no_record">No Record</div>):('')}
                    
                </div>
            </div>

        </div>
        <div className="container-fluid my-bet-overlay-bg">
            <div className="container py-5">
                <div className="row">
                    <div className="col-12  my-5">
                        <h3 className="my-current-problems">
                            <a href="https://www.ncpgambling.org/help-treatment/national-helpline-1-800-522-4700/" target="_blank" className="text-white">
                                If you have a Gambling Problem?
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>

        {/* <!-- betting model --> */}
        <div className="modal fade betNowModal" id="betNow_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Bet Now</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form onSubmit={bet_now_submit}>
                            <div className="modal-body">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="">Enter Betting Amount ($)</label>
                                                <input type="text" className="form-control" name="bet_amount" id="bet_amount" placeholder="Enter bet amount" />
                                                {/* <input type="hidden" className="form-control" name="user_id" id="user_id" /> */}
                                                <input type="hidden" className="form-control" name="game_id" id="game_id" />
                                                <input type="hidden" className="form-control" name="bet_team" id="bet_team" />
                                                <input type="hidden" className="form-control" name="american_ods" id="american_ods" />
                                                <input type="hidden" className="form-control" name="decimal_ods" id="decimal_ods" />
                                                <input type="hidden" className="form-control" name="fractional_ods" id="fractional_ods" />
                                                <input type="hidden" className="form-control" name="bet_box" id="bet_box" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlSelect1">Bet on team</label>
                                                {/* <select className="form-control" name="bet_team" id="bet_team">
                                                </select> */}
                                                <input type="text" className="form-control" id="bet_team1" readOnly />
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="placebet_btn">Place Bet</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
        {/* <!-- //betting model --> */}
    </section>
    <Footer/>
   
</section>

    
    
    
    
    
    
    </>
    )
}
export default games;