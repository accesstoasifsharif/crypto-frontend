import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import axios from "axios";
import $ from 'jquery';
import Swal from "sweetalert2";
import UserAuth from '../user_auth';
let UserCheckAuthObj = new UserAuth();

 function login() {
	const router = useRouter();
    useEffect(() => {
        if(!router.isReady) return;
        //hideLoading();
        UserCheckAuthObj.UserCheckAuthLogin({router});
    }, [router.isReady]);

    const loginUser = async event => {
        event.preventDefault()
        if(event.target.email.value=='' || event.target.password.value==''){
            var alert_message = 'Please Fill These Fields: <br>';
            var newLine = "\r\n";
            alert_message += newLine;
            if(event.target.email.value==''){
                alert_message += "Username can not be left blank. <br>";
                alert_message += newLine;
            }
            if(event.target.password.value==''){
                alert_message += "Password can not be left blank. <br>";
                alert_message += newLine;
            }
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              //text: alert_message
              html: alert_message
              //footer: '<a href="">Why do I have this issue?</a>'
            })
            return false;
        }
        
        
        
        var data = new Object();
        data.email = event.target.email.value;
        data.password = event.target.password.value;
        var config = {
            method: 'post',
            url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/login`,
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            data : data
          };
          
          axios(config)
          .then(function (response) {
            if(response.data.hasOwnProperty('message')){
                if(response.data.message == 'Authenticated'){
                    //if(Object.keys(response.data.user).length !== 0 && response.data.user.constructor === Object){
                    if(response.data.message_title == 'success'){
                        localStorage.setItem("op_user",JSON.stringify(response.data.user));
                        //console.log(response.data);
                        swalAutoclose('success', 'Logged In successfully', 1000);
                        router.push("/dashboard");
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: response.data.message_description
                        })
                    }
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: "Invalid credentials"
                    })
                }
                
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "Invalid credentials"
                })
            }
            
          })
          .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: "Invalid credentials"
            })
            //console.log(error);
          });
          


    }

    const swalAutoclose = (title, html, timer, timer_progress_bar=false)=>{
        let timerInterval
        return Swal.fire({
            title: title,
            html: html,
            timer: timer,
            timerProgressBar: timer_progress_bar,
            didOpen: () => {
                Swal.showLoading()
                //const b = Swal.getHtmlContainer().querySelector('b')
                // timerInterval = setInterval(() => {
                // b.textContent = Swal.getTimerLeft()
                // }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                //console.log('I was closed by the timer')
            }
        })
    }
  return (
	  <>
    <div className="login_box_outer">
		<div className="login_box_inner">
			<div className="login_header">
				<h1>OPSportsBook{/* {process.env.NEXT_PUBLIC_API_DOMAIN} */}</h1>
				<span><Link  href="/register">Sign Up</Link></span>
			</div>
			<div className="login_body">
				<form onSubmit={loginUser}>
					<div className="form-group">
						<label>Email or Account Id</label>
						<input type="email" name="email" id="email" className="form-control" placeholder="Enter email"/>
					</div>
					<div className="form-group">
						<label className="password_label">Password</label>
						<input type="password" name="password" id="password" className="form-control" placeholder="Enter password"/>
						<small><Link href="/forgotpassword"><a className="forgot_password_a">Forget Password?</a></Link></small>
					</div>

					<div className="login_btn_div">
						<button type="submit" className="login_btn_a">LOG IN</button>
						<small><a href="#" className="support_a">Get help in logging in? Support Team</a></small>
					</div>
				</form>
			</div>
		</div>
	</div>
	</>
  
  )
}
export default login ;