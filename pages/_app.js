import Head from "next/head";
import Script from "next/script";
import "../styles/globals.css";
import { useEffect } from "react";
import { useRouter } from "next/router";
import $ from 'jquery';
import NProgress from 'nprogress';
import '../node_modules/nprogress/nprogress.css';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../public/css/login_style.css";

import "../public/css/main_style.css";
import "../public/css/my-bet.css";
import "../public/css/register-style.css";
import "../public/css/my-profile.css";
import "../public/css/user-profile.css";
import "../public/css/mybet-popup.css";
import "../public/css/homepage_style.css";
import "../public/css/swapToken.css";

//import "../public/css/game-list.css";
import { ContextProvider } from "../components/wallet/ContextProvider";
import { createStore, compose, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import reducers from "../store/reducers";
require("@solana/wallet-adapter-react-ui/styles.css");
const store = createStore(reducers, compose(applyMiddleware(thunk)));

NProgress.configure({ showSpinner: false });
function MyApp({ Component, pageProps }) {
	useEffect(() => {
		import("../node_modules/jquery/dist/jquery.min.js");
		import("../node_modules/jquery/dist/jquery.slim.min.js");
		import("../node_modules/popper.js/dist/popper.js");
		import("../node_modules/bootstrap/dist/js/bootstrap.js");
		import("../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js");
		//import $ from '../node_modules/jquery/dist/jquery'
		// import "../public/js/main_slider.js";
		// import "../public/js/search_bar.js";
		// import ("../public/js/main_slider")
		// import ("../public/js/search_bar")
	}, []);

	return (
		<>
			<Head>
				{/* <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0"
        />
        <meta data-react-helmet="true" name="docsearch:version" content="2.0" />
        <meta
          data-react-helmet="true"
          name="viewport"
          content="width=device-width,initial-scale=1,shrink-to-fit=no,viewport-fit=cover,user-scalable=0"
        /> */}

				{/* <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          rel="styleSheet"
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap"
        />

        <link
          rel="styleSheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"
        />

       
        <link rel="styleSheet" type="text/css" href="css/register-style.css" /> */}

				{/*  <link
          rel="styleSheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        />
       
        <link rel="styleSheet" type="text/css" href="css/my-bet.css" />
       
       
        
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        /> */}

				{/* <link rel="styleSheet" type="text/css" href="css/main_style.css" />
       
       
        
      

        <link
          rel="styleSheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        /> */}

				{/* <link
          rel="styleSheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
        />
        <link
          rel="styleSheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
        /> */}

				{/* <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="styleSheet"
        /> */}

				{/* <link rel="styleSheet" type="text/css" href="css/my-profile.css" />
       
        <link rel="styleSheet" type="text/css" href="css/user-profile.css" />
 */}

				{/* <link rel="styleSheet" type="text/css" href="css/homepage_style.css" />
        <link rel="styleSheet" href="css/mybet-popup.css" /> */}

				<link
					rel="styleSheet"
					href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
				/>

				<link
					rel="stylesheet"
					href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
				/>
				<link
					rel="stylesheet"
					href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
				/>
			</Head>

			<>
				<Script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" />
				<Script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" />

				{/* <Script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js'/> */}
				{/* <Script type="text/javascript" src="js/main_slider.js" /> */}
				<Script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" />
				{/* <Script type="text/javascript" src="js/serach_bar.js"/>
    <Script type="text/javascript" src="js/main_slider.js"/> */}

				<Script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" />

				<Provider store={store} className="image-main-container">
					<ContextProvider>
						<Component {...pageProps} />
					</ContextProvider>
				</Provider>
			</>

			{/* <Script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossOrigin="anonymous"/> */}
		</>
	);
}

export default MyApp;
