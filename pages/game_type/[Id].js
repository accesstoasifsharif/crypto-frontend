import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from 'next/link';

import Footer from '../../components/layout/Footer';
import Header from '../../components/layout/Header';
import Navbar from '../../components/layout/Navbar';

import axios from "axios";
import rateLimit from 'axios-rate-limit';
import $ from 'jquery';
import Swal from "sweetalert2";
import UserAuth from '../../user_auth';
let UserCheckAuthObj = new UserAuth();
import EncryptionClass from '../../Encryption';
let Encryption = new EncryptionClass();

 function game() {
    const router = useRouter();
    const [op_user, set_op_user] = useState(null);
    const [games_upcoming, set_games_upcoming] = useState(null);
    const [games_upcoming_p, set_games_upcoming_p] = useState([]);
    const [admin_cms_by_page_bet, set_admin_cms_by_page_bet] = useState(null);

    useEffect(() => {
        if(!router.isReady) return;
        //UserCheckAuthObj.UserCheckAuth({router});
        var user_from_local_storage = getUserFromLocalStorage();
        set_op_user(user_from_local_storage);
        
        
        var {Id} = router.query;
		Id  = Encryption.encrypt_decrypt('decrypt',Id.toString());
        //console.log(Id)

        const myResponse = [];
        //if(Object.keys(user_from_local_storage).length !== 0 && user_from_local_storage.constructor === Object){
            retrieveGameData(parseInt(Id)/* user_from_local_storage.id, user_from_local_storage.remember_token */, "Bet")
            .then(
            axios.spread((...response) => {
                for (let i = 0; i <= 1; i += 1) {
                    myResponse.push(response[i].data)
                }
                //console.log(response)
                if(myResponse[0].results.length != 0){
                    set_games_upcoming(myResponse[0].results);
                    set_games_upcoming_p(myResponse[0].results);
                    const data = { "req_per_page": document.getElementById("req_per_page").value, "page_no": 1 };
                    pagination(data, myResponse[0].results);
                }  
                if(myResponse[1].results.length != 0) set_admin_cms_by_page_bet(myResponse[1].results);
                //set_games_upcoming(myResponse[0].results);
                
               
            })
            )
            .finally(() => {
                // Update refreshToken after 3 seconds so this event will re-trigger and update the data
                //setTimeout(() => setRefreshToken(Math.random()), 15000);
            });
          //}
        
    }, [router.isReady]);
    
    function getUserFromLocalStorage(){
        return JSON.parse(localStorage.getItem('op_user')) || [];
    }
    const jsonBetsArrayFilterByTimeline = (arr, timeline)=>{
        return  arr.filter(function(arr_v){
             return arr_v.timeline == timeline;         
         })
     }
    async function retrieveGameData(game_type/* user_id, remember_token */, page) {
        const http = rateLimit(
            axios.create({headers: { 
                'Accept': 'application/json'/* , 
                'Authorization': 'Bearer '+remember_token */
            }
            }),
            {maxRequests: 3, perMilliseconds: 1000, maxRPS: 3}
        );
        const res = await axios.all([
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/games_by_game_type?timeline=0&game_type=${game_type}`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/admin_cms_by_page?page=${page}`)/* ,
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=1`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=2`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/games?timeline=0`) */
        ])
        
        return res;
    }
    const paginator = (items, current_page, per_page_items) => {
        //function paginator(items, current_page, per_page_items) {
            //console.log([typeof current_page, typeof per_page_items]);
            let page = current_page || 1,
            per_page = per_page_items || 10,
            offset = (page - 1) * per_page,
        
            paginatedItems = items.slice(offset).slice(0, per_page_items),
            total_pages = Math.ceil(items.length / per_page);
        
            return {
                page: page,
                per_page: per_page,
                pre_page: page - 1 ? page - 1 : null,
                next_page: (total_pages > page) ? page + 1 : null,
                total: items.length,
                total_pages: total_pages,
                data: paginatedItems
            };
        }
    
        // Pagination logic implementation
        const pagination = (data, all_data) => {
            
            $("#games_upcoming_pagination").empty();
            if (data.req_per_page !== 'ALL') {
                const myarr = paginator(all_data, data.page_no, parseInt(data.req_per_page));
                //console.log(myarr);
                const string_all_data  = JSON.stringify(all_data);
                let pager = `<a id="prev_link" class="games_upcoming_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
                    `<a class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;
                
                for (let num = 2; num <= myarr.total_pages; num++) {
                    pager += `<a  class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
                }
                pager += `<a id="next_link"  class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;
                
                $("#games_upcoming_pagination").append(pager);
                
                $('div#games_upcoming_pagination a').each(function(){
                    if(this.innerHTML == myarr.page){
                        $(this).addClass('active');
                    }
                });
    
                pagination_initialize('games_upcoming_p_btn','req_per_page');
                set_games_upcoming(myarr.data);
                
            } else {
                const myarr = paginator(all_data, data.page_no, all_data.length);
                //console.log(myarr);
                const string_all_data  = JSON.stringify(all_data);
                set_games_upcoming(myarr.data);
            }
            
        }

        // trigger when requests per page dropdown changes
    const pagination_initialize = (pagination_btn,req_per_page_select) => {
        document.querySelectorAll('.'+pagination_btn).forEach(btn => {
            btn.addEventListener('click', () => {
                const page_no = btn.getAttribute("data-id");
                if(page_no !== 'null'){
                    const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": parseInt(page_no) };
                    pagination(data, JSON.parse(btn.getAttribute("data-value")));
                }
                
            });
        });
    }
    const filter_requests = async (games_upcoming_p,req_per_page_select) => {
        const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": 1 };
        pagination(data, games_upcoming_p);
    }

    const bet_now = async (upcoming_games_row_v) => {
        router.push("/games");
        //console.log([upcoming_games_row_v,op_user]);
        // if(Object.keys(op_user).length !== 0 && op_user.constructor === Object){
        //     $('#bet_team').empty();
        //     $('#bet_team').append($('<option>').text(upcoming_games_row_v.team1detail.name).attr('value', 1));
        //     $('#bet_team').append($('<option>').text(upcoming_games_row_v.team2detail.name).attr('value', 2));
        //     $("#game_id").val(upcoming_games_row_v.id);
        //     $("#user_id").val(op_user.id);
        //     $('#betNow_modal').modal('toggle');
        // }else{
        //     router.push("/login");
        // }
        
    }
    const bet_now_submit = async event => {
        event.preventDefault();
        console.log([event.target.user_id.value, event.target.game_id.value,event.target.bet_amount.value,event.target.bet_team.value]);
        
        var alert_message = 'Please Fill These Fields: <br>';
        var newLine = "\r\n";
        alert_message += newLine;
        
      if(event.target.user_id.value=='' || event.target.game_id.value=='' || event.target.bet_amount.value=='' || event.target.bet_team.value==''){
        
        if(event.target.user_id.value==''){
            alert_message += "User Id can not be left blank. <br>";
            alert_message += newLine;
        }
        if(event.target.game_id.value==''){
          alert_message += "Game Id can not be left blank. <br>";
          alert_message += newLine;
        }
        if(event.target.bet_amount.value==''){
            alert_message += "Amount can not be left blank. <br>";
            alert_message += newLine;
        }
        if(event.target.bet_team.value==''){
            alert_message += "Team can not be left blank. <br>";
            alert_message += newLine;
        }
        
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          //text: alert_message
          html: alert_message
          //footer: '<a href="">Why do I have this issue?</a>'
        })
        return false;
    }
    
     
        var data = new Object();
        data.user_id = event.target.user_id.value;
        data.game_id = event.target.game_id.value;
        data.bet_amount = event.target.bet_amount.value;
        data.bet_team = event.target.bet_team.value;
        var config = {
            method: 'post',
            url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets/add`,
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+op_user.remember_token
            },
            data : data
          };
          
          axios(config)
          .then(function (response) {
            if(response.data.hasOwnProperty('message')){
                if(response.data.message == 'Authenticated'){
                    //console.log(response.data);
                    if(response.data.message_title == 'success'){
                        // set_bets_upcoming(response.data.results);
                        // set_bets_upcoming_p(response.data.results);
                        // const data = { "req_per_page": document.getElementById("bets_upcoming_req_per_page").value, "page_no": 1 };
                        // bets_upcoming_pagination(data, response.data.results);
                        swalAutoclose('success', 'Bet Placed Successfully', 1000);
                    }else{
                        Swal.fire('Unable to bet '+response.data.results);
                    }
                    
                }else{
                    swalAutoclose('error', 'Unable to bet', 1000)
                }
                
            }else{
                swalAutoclose('error', 'Unable to bet', 1000)
            }
            
          })
          .catch(function (error) {
              swalAutoclose('error', 'Unable to bet', 1000)
          });
    }
    const swalAutoclose = (title, html, timer, timer_progress_bar=false)=>{
        let timerInterval
        return Swal.fire({
            title: title,
            html: html,
            timer: timer,
            timerProgressBar: timer_progress_bar,
            didOpen: () => {
                Swal.showLoading()
                //const b = Swal.getHtmlContainer().querySelector('b')
                // timerInterval = setInterval(() => {
                // b.textContent = Swal.getTimerLeft()
                // }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                //console.log('I was closed by the timer')
            }
        })
    }
    //console.log(games_upcoming);
  return (
    <>
    <style global jsx>{`
    .text-center.pagination_no_record {
        color: #fff;
    }
    `}</style>
    <section className="my-bet-top-bg">
  
    <Header /* data={op_user} */ />


    <Navbar /* data={op_user} */ />




    <section>
        <div className="container-fluid ">
            <div className="container py-2">
                <div className="row py-3">
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="/images/industrial.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12  icn-content">
                                <p className="top-banner-heading">
                                    INDUSTRY FIRST
                                </p>
                                <p className="top-banner-desc"> Worlds 1st Web 3.0 Sportsbook</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="/images/alarm-on.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12 icn-content">
                                <p className="top-banner-heading">
                                    FASTEST PAYOUTS
                                </p>
                                <p className="top-banner-desc"> Winnings automatically airdropped to wallet with no fee</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 py-2">
                        <div className="row">
                            <div className="col-md-1 p-0 col-sm-1 col-12 icn-content">
                                <img src="/images/welcome.svg" alt="" className="profile-icn"/>
                            </div>
                            <div className="col-md-11 col-sm-11 col-12 icn-content">
                                <p className="top-banner-heading">
                                    BET WITH STABLE CRYPTO
                                </p>
                                <p className="top-banner-desc">Never worry about the volatility of crypto when beting</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container-fluid  ">
            <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">

                <div className="carousel-inner">
                    {admin_cms_by_page_bet && admin_cms_by_page_bet.map((admin_cms_by_page_bet_row_v, admin_cms_by_page_bet_row_i)=>(
                        <div key={admin_cms_by_page_bet_row_i} className={admin_cms_by_page_bet_row_i == 0 ? 'carousel-item active':'carousel-item'}>
                            <img src={`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_bet_row_v?.cms_image}`} className="d-block w-100 p-4 my-bet-carousel-img" alt="..."/>
                            <div className="" dangerouslySetInnerHTML={{ __html: admin_cms_by_page_bet_row_v.cms_desc }} />
                        </div>
                    ))}
                    
                    {/* <div className="carousel-item">
                        <img src={ (admin_cms_by_page_bet) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_bet[1]?.cms_image}`):('/images/banner.webp')} className="d-block w-100 p-4 my-bet-carousel-img" alt="..."/>
                        <div className="carousel-caption  my-bet-carousel-caption">
                            <button className="my-bet-carousel-caption-button my-4"><a href="/promotion">GET DEPOSIT</a></button>
                        </div>
                    </div> */}
                </div>
                <button className="carousel-control-prev my-bet-left-btn" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
                    <i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
   
                </button>
                <button className="carousel-control-next my-bet-right-btn" type="button" data-target="#carouselExampleCaptions" data-slide="next">
                    <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
                </button>
            </div>
        </div>

        <div className="container-fluid">
            <div className="container">
                <div className="row">
                    <div className="col-12 my-current-bets my-5">
                        <h3>Current Game List</h3>
                    </div>
                </div>
                <div className="row py-5 game_list_row">
                {games_upcoming && games_upcoming.map((games_upcoming_row_v,games_upcoming_row_i) => (
                    <div key={games_upcoming_row_i} className="col-xl-12 col-sm-12 col-md-12">
                        <div className="card current-active-bets my-4 ">
                            <div className="card-body current-active-bets">
                                <div className="row">
                                    <div className="col-md-2 user-upcoming-games-bgg" style={{backgroundImage: `url(${process.env.NEXT_PUBLIC_FILE_PATH}games_type/${games_upcoming_row_v.game_type_details.img})`}}>

                                    </div>
                                    <div className="col-md-3 text-center game-type">
                                        <button type="button" className="btn game-type-btn">Game Type</button>
                                        <h6 className="bet-topics">{games_upcoming_row_v.game_type_name}</h6>
                                    </div>
                                    <div className="col-md-7 game_betting_box">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <p className="price">
                                                    Team 1
                                                </p>
                                            </div>
                                            <div className="col-md-6">
                                                <p className="price-digit">{games_upcoming_row_v.team1detail.name}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6">
                                                <p className="price">
                                                    Team2
                                                </p>
                                            </div>
                                            <div className="col-md-6">
                                                <p className="price-digit">{games_upcoming_row_v.team2detail.name}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6">
                                                <p className="price">
                                                    Venue
                                                </p>
                                            </div>
                                            <div className="col-md-6">
                                                <p className="price-digit">{games_upcoming_row_v.venue}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6">
                                                <p className="price">
                                                    Maximum Betting Amount
                                                </p>
                                            </div>
                                            <div className="col-md-6">
                                                <p className="price-digit">{'$'+games_upcoming_row_v.max_amount_user_bet.toFixed(2)}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                {/* <a href="#" className="betnow_btn" data-toggle="modal" data-target="#betNow_modal">Bet Now</a> */}
                                                {/* <a className="betnow_btn" data-toggle="tooltip" title="Bet Now" onClick={()=>bet_now(games_upcoming_row_v)}>Bet Now</a> */}
                                                {games_upcoming_row_v?.timeline == '0' ? (<span className="betnow_btn" data-toggle="tooltip" title="Bet Now" style={{cursor:"none", backgroundColor:"unset"}}>Start soon</span>):(<Link href="/games/[Id]" as={`/games/${Encryption.encrypt_decrypt('encrypt',games_upcoming_row_v.id.toString())}`}><a className="betnow_btn" data-toggle="tooltip" title="Bet Now">Bet Now</a></Link>)}
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                ))}

                    
                    <div className="paginator_btn">
                        <div className="clearfix">
                            <div className="box options">
                                <label>Requests Per Page: </label>
                                <select id="req_per_page" onChange={() => filter_requests(games_upcoming_p,'req_per_page')}>
                                    <option>5</option>
                                    <option>1</option>
                                    <option>10</option>
                                    <option>ALL</option>
                                </select>
                            </div>
                            <div id="games_upcoming_pagination" className="box pagination">
                            </div>
                        </div>
                        {games_upcoming==null ? (<div className="text-center pagination_no_record">No Record</div>):('')}
                    </div>

                    
                    
                </div>


                {/* <div className="card mx-auto  my-live-bet-matches my-5">
                    <div className="card-body">
                        <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <h5 className=""><button className="live-button"></button>Live 3rd</h5>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">SPREAD</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                            </div>
                        </div>
                        <div className="row card-title-list">
                            <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                <p className="my-live-bet-matches-list">Fc Barcelona vs Real Madrid</p>
                            </div>
                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                <p className="my-live-bet-matches-list">Fc Barcelona</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <p className="my-live-bet-matches-list">Real Madrid</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <a href="#" className="match-your-teams">Get Preep</a>
                        </div>
                    </div>
                </div>
                <div className="card mx-auto  my-live-bet-matches my-5">
                    <div className="card-body">
                        <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <h5 className=""><button className="live-button"></button>Live 3rd</h5>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">SPREAD</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                            </div>
                        </div>
                        <div className="row card-title-list">
                            <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                <p className="my-live-bet-matches-list">Fc Barcelona vs Real Madrid</p>
                            </div>
                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                <p className="my-live-bet-matches-list">Fc Barcelona</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <p className="my-live-bet-matches-list">Real Madrid</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <a href="#" className="match-your-teams">Get Preep</a>
                        </div>
                    </div>
                </div>
                <div className="card mx-auto  my-live-bet-matches my-5">
                    <div className="card-body">
                        <div className="row my-live-bet-matches-heading my-live-bet-matches-card-title">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <h5 className=""><button className="live-button"></button>Live 3rd</h5>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">SPREAD</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">MONEYLINE</p>
                            </div>
                            <div className="col-md-2 d-flex align-items-center p-0 col-sm-12 col-12 live-bet-matches justify-content-center">
                                <p className="my-live-bet-matches-tabs">OVER/UNDER</p>
                            </div>
                        </div>
                        <div className="row card-title-list">
                            <div className="col-md-12 col-sm-12 col-12 live-bet-matches pt-1 pb-1">
                                <p className="my-live-bet-matches-list">Fc Barcelona vs Real Madrid</p>
                            </div>
                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches ">
                                <p className="my-live-bet-matches-list">Fc Barcelona</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12 col-12 live-bet-matches">
                                <p className="my-live-bet-matches-list">Real Madrid</p>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                            <div className="col-md-2 col-sm-12 col-12 live-bet-matches">
                                <button type="button" className="btn my-bet-team mt-2"></button>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <a href="" className="match-your-teams">Get Preep</a>
                        </div>
                    </div>
                </div> */}
            </div>
        </div>
        <div className="container-fluid my-bet-overlay-bg">
            <div className="container py-5">
                <div className="row">
                    <div className="col-12  my-5">
                        <h3 className="my-current-problems">
                            <a href="https://www.ncpgambling.org/help-treatment/national-helpline-1-800-522-4700/" target="_blank" className="text-white">
                                If you have a Gambling Problem?
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>

        {/* <!-- betting model --> */}
        <div className="modal fade betNowModal" id="betNow_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Bet Now</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form onSubmit={bet_now_submit}>
                            <div className="modal-body">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="">Enter Betting Amount ($)</label>
                                                <input type="text" className="form-control" name="bet_amount" id="bet_amount" placeholder="Enter bet amount" />
                                                <input type="hidden" className="form-control" name="user_id" id="user_id" />
                                                <input type="hidden" className="form-control" name="game_id" id="game_id" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlSelect1">Bet on team</label>
                                                <select className="form-control" name="bet_team" id="bet_team">
                                                  {/* <option value="1">Team1</option>
                                                  <option value="2">Team2</option> */}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="placebet_btn">Place Bet</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
        {/* <!-- //betting model --> */}

    </section>
    <Footer/>
   
</section>

    
    
    
    
    
    
    </>
    )
}
export default game;