import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from 'next/link';

import axios from "axios";
import $ from 'jquery';
import Swal from "sweetalert2";
import UserAuth from '../user_auth';
let UserCheckAuthObj = new UserAuth();

function register() {
    const router = useRouter();
    const [op_user, set_op_user] = useState(null);

    useEffect(() => {
        if(!router.isReady) return;
        UserCheckAuthObj.UserCheckAuthRegister({router});
        var user_from_local_storage = getUserFromLocalStorage();
        set_op_user(user_from_local_storage);
        
    }, [router.isReady]);
    
    function getUserFromLocalStorage(){
        return JSON.parse(localStorage.getItem('op_user')) || [];
    }

    const registerUser = async event => {
      event.preventDefault()
        var alert_message = '';
        var newLine = "\r\n";
        alert_message += newLine;
        if(event.target.password.value !== event.target.password_confirmation.value){
          alert_message += "Password & Confirm password didn't match. <br>";
          alert_message += newLine;
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            //text: alert_message
            html: alert_message
            //footer: '<a href="">Why do I have this issue?</a>'
          })
          return false;
        }
      if(event.target.name.value=='' || event.target.email.value=='' || event.target.password.value=='' || event.target.password_confirmation.value=='' || event.target.account_id.value==''){
        alert_message = 'Please Fill These Fields: <br>';
        var newLine = "\r\n";
        alert_message += newLine;
        if(event.target.password.value !== event.target.password_confirmation.value){
          alert_message += "Password & Confirm password didn't match. <br>";
          alert_message += newLine;
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            //text: alert_message
            html: alert_message
            //footer: '<a href="">Why do I have this issue?</a>'
          })
          return false;
        }

        if(event.target.name.value==''){
            alert_message += "Name can not be left blank. <br>";
            alert_message += newLine;
        }
        if(event.target.email.value==''){
          alert_message += "Email can not be left blank. <br>";
          alert_message += newLine;
      }
        if(event.target.password.value==''){
            alert_message += "Password can not be left blank. <br>";
            alert_message += newLine;
        }
        if(event.target.password_confirmation.value==''){
          alert_message += "Password confirmation can not be left blank. <br>";
          alert_message += newLine;
        }
        if(event.target.account_id.value==''){
          alert_message += "Account Id can not be left blank. <br>";
          alert_message += newLine;
        }
        // if(event.target.phone_number.value==''){
        //   alert_message += "Phone number can not be left blank. <br>";
        //   alert_message += newLine;
        // }
        
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          //text: alert_message
          html: alert_message
          //footer: '<a href="">Why do I have this issue?</a>'
        })
        return false;
    }
    
     
        var data = new Object();
        data.name = event.target.name.value;
        data.email = event.target.email.value;
        data.password = event.target.password.value;
        data.password_confirmation = event.target.password_confirmation.value;
        data.account_id = event.target.account_id.value;
        //data.phone_number = event.target.phone_number.value;
        var config = {
            method: 'post',
            url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/register`,
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            data : data
          };
          
          axios(config)
          .then(function (response) {
            if(response.data.hasOwnProperty('message')){
                if(response.data.message == 'Authenticated'){
                    //if(Object.keys(response.data.user).length !== 0 && response.data.user.constructor === Object){
                    if(response.data.message_title == 'success'){
                        localStorage.setItem("op_user",JSON.stringify(response.data.user));
                        swalAutoclose('success', 'Registered successfully', 1000);
                        router.push("/dashboard");
                    }else{
                      alert_message = '<br>';
                      var obj = response.data.message_description;
                      for (var key in obj) {
                        if (obj.hasOwnProperty(key)) {
                          var val = obj[key];
                          alert_message += val;
                          alert_message += '<br>';
                        }
                      }
                        //swalAutoclose('error', JSON.stringify(response.data.message_description), 1000)
                        Swal.fire('error', alert_message);
                    }
                }else{
                    swalAutoclose('error', 'Unable to register', 1000)
                }
                
            }else{
                swalAutoclose('error', 'Unable to register', 1000)
            }
            
          })
          .catch(function (error) {
              swalAutoclose('error', 'Unable to register', 1000)
          });
    }    

    const swalAutoclose = (title, html, timer, timer_progress_bar=false)=>{
      let timerInterval
      return Swal.fire({
          title: title,
          html: html,
          timer: timer,
          timerProgressBar: timer_progress_bar,
          didOpen: () => {
              Swal.showLoading()
              //const b = Swal.getHtmlContainer().querySelector('b')
              // timerInterval = setInterval(() => {
              // b.textContent = Swal.getTimerLeft()
              // }, 100)
          },
          willClose: () => {
              clearInterval(timerInterval)
          }
          }).then((result) => {
          /* Read more about handling dismissals below */
          if (result.dismiss === Swal.DismissReason.timer) {
              //console.log('I was closed by the timer')
          }
      })
  }
  return (
    <>
      <nav className="navbar my-navbar navbar-expand-lg  ">
        <div className="container-fluid justify-content-center">
          <div className="row w-100">
            <div className="col-lg-4 col-sm-12 py-2 py-lg-0 ">
              <Link href="/">
              <a className="navbar-brand register-nav-link">
                OPSportsbook
              </a>
              </Link>
            </div>
            <div className="col-lg-4 col-sm-12 py-2 py-lg-0">
              {" "}
              <h3  className=" register-create text-center">
                Create Your Account
              </h3>
            </div>
            <div className="col-lg-4 col-sm-12 py-2 py-lg-0 ">
              <div className="" id="navbarNav">
                <ul className="navbar-nav register-nav-login-link">
                  <li className="nav-item nav-active ">
                  <Link href="/login">
                    <button type="button" className="btn register-login  register-nav-link " >
                      Login
                    </button>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>

      <section>
      <form onSubmit={registerUser}>
        <div className="container-fluid left-register-bg">
          <div className="row">
          
            <div className="col-md-12 left-register-bg ">
              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-lg-4 col-sm-8 mt-lg-5 mt-0">
                    
                      <div className="form-group py-lg-3 p-1 m-1">
                        <label className="register-account-id">
                          Name
                        </label>
                        <input
                          type="text"
                          className="form-control register-account-id-field"
                          name="name" id="name"
                          required
                        />
                      </div>
                      <div className="form-group py-lg-3 p-1 m-1">
                        <label className="register-account-id">
                          Account ID
                        </label>
                        <input
                          type="text"
                          className="form-control register-account-id-field"
                          name="account_id" id="account_id"
                          required
                        />
                      </div>
                      <div className="form-group py-lg-3 p-1 m-1">
                        <label className="register-email-address">
                          Email ID
                        </label>
                        <input
                          type="email"
                          className="form-control register-email-address-field"
                          name="email"
                          id="email"
                        />
                      </div>
                      
                    
                  </div>
                  <div className="col-lg-4 col-sm-8 mt-lg-5 mt-0">
                  {/* <div className="form-group py-lg-3 p-1 m-1">
                        <label className="register-phone-number">
                          Phone Number
                        </label>
                        <input
                          type="tel"
                          id="phone_number"
                          name="phone_number"
                          className="form-control register-phone-number-field"
                        />
                      </div> */}
                      <div className="form-group py-lg-3 p-1 m-1">
                        <label className="form-label register-password">
                          Password
                        </label>
                        <input
                          type="password"
                          className="form-control register-password-field"
                          name="password"
                          id="password"
                        />
                      </div>
                      <div className="form-group py-lg-3 p-1 m-1">
                        <label className="form-label register-password">
                          Confirm Password
                        </label>
                        <input
                          type="password"
                          className="form-control register-password-field"
                          name="password_confirmation"
                          id="password_confirmation"
                        />
                      </div>
                  </div>
                  {/* <div className="col-lg-4 col-sm-8 mt-5 ml-lg-5"></div> */}
                </div>
              </div>
            </div>
            {/* <div className="col-md-4 right-register-bg right-register-bg-mob"></div> */}
            <div className="col-md-12 text-center ">
              <div className="submit">
                <button type="submit" className="btn register-login my-3">
                  SIGN UP
                </button>
              </div>
            </div>
            
          </div>
        </div>
        </form>
      </section>

      <footer className="spc_footer">
        <div className="container-fluid">
          <div className="row   m-0">
            <div className="col-md-2"></div>
            <div className="col-md-2 p-3">
              <a href="#" className="register-faq link_set">
                FAQ 
              </a>
              <p className="quick-links-register my-2">How do I access it? </p>
              <p className="quick-links-register my-2 ">
                How do editions function?{" "}
              </p>
              <p className="quick-links-register my-2 ">
                {" "}
                Can I Withdraw Coins?{" "}
              </p>
              <p className="quick-links-register my-2 ">
                {" "}
                How I Access my account?{" "}
              </p>
              <p className="quick-links-register my-2 ">
                {" "}
                How bet will proceed?{" "}
              </p>
              <p className="quick-links-register my-2 ">
                How Troubleshoot account?
              </p>
            </div>
            <div className="col-md-2 p-3">
              <a href="" className="register-contact link_set">
                CONTACT{" "}
              </a>
              <p className="quick-links-register my-1">info@</p>
              <p className="quick-links-register my-1">Office</p>
              <p className="quick-links-register my-1">00-(0000)00</p>
            </div>

            <div className="col-md-2 p-3">
              <a href="" className="register-twitter link_set">
                TWITTER{" "}
              </a>
            </div>
            <div className="col-md-2 p-3">
              <a href="" className="register-discord link_set">
                DISCORD
              </a>
            </div>
            <div className="col-md-2"></div>
          </div>
        </div>
      </footer>
    </>
  );
}
export default register;
