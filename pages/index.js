import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import Header from "../components/layout/Header";
import Navbar from "../components/layout/Navbar";
import Footer from "../components/layout/Footer";
import axios from "axios";
import rateLimit from "axios-rate-limit";
import $ from "jquery";
import Swal from "sweetalert2";

import EncryptionClass from '../Encryption';
let Encryption = new EncryptionClass();

function homepage() {
	const router = useRouter();
	const [op_user, set_op_user] = useState(null);
	const [game_type, set_game_type] = useState(null);
	const [admin_cms_by_page_home, set_admin_cms_by_page_home] = useState(null);

	useEffect(() => {
		if (!router.isReady) return;
		var user_from_local_storage = getUserFromLocalStorage();
		set_op_user(user_from_local_storage);
		const myResponse = [];
		//if(Object.keys(user_from_local_storage).length !== 0 && user_from_local_storage.constructor === Object){
		retrieveGameData(user_from_local_storage.remember_token, "Home")
			.then(
				axios.spread((...response) => {
					for (let i = 0; i <= 1; i += 1) {
						myResponse.push(response[i].data);
					}
					//console.log(response)
					if (myResponse[0].results.length != 0)
						set_game_type(myResponse[0].results);
					if (myResponse[1].results.length != 0)
						set_admin_cms_by_page_home(myResponse[1].results);
				})
			)
			.finally(() => {
				// Update refreshToken after 3 seconds so this event will re-trigger and update the data
				//setTimeout(() => setRefreshToken(Math.random()), 15000);
			});
		//}
	}, [router.isReady]);

	function getUserFromLocalStorage() {
		return JSON.parse(localStorage.getItem("op_user")) || [];
	}
	const jsonBetsArrayFilterByTimeline = (arr, timeline) => {
		return arr.filter(function (arr_v) {
			return arr_v.timeline == timeline;
		});
	};
	async function retrieveGameData(remember_token, page) {
		const http = rateLimit(
			axios.create({
				headers: {
					Accept: "application/json",
					Authorization: "Bearer " + remember_token,
				},
			}),
			{ maxRequests: 3, perMilliseconds: 1000, maxRPS: 3 }
		);
		const res = await axios.all([
			http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/game_type`),
			http.post(
				`${process.env.NEXT_PUBLIC_API_DOMAIN}user/admin_cms_by_page?page=${page}`
			) /*,
          http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=2`),
          http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/games?timeline=0`) */,
		]);
		return res;
	}

	return (
		<>
			<Header /* data={op_user} */ />

			<Navbar /* data={op_user} */ />
			{/*//header*/}

			{/*- homepage*/}
			{/* 3 columns section */}

			<section className="three_col_sec">
				<div className="container-fluid">
					<div className="row">
						<div className="col-md-4 col-sm-12 col-xl-4 col-12">
							<div className="col_box">
								<div className="icon_box">
									<img
										src="images/industrial.webp"
										className="profile-icn"
									/>
								</div>
								<div className="text_box">
									<h2>INDUSTRY FIRST</h2>
									<p>Worlds 1st Web 3.0 Sportsbook</p>
								</div>
							</div>
						</div>

						<div className="col-md-4 col-sm-12 col-xl-4 col-12">
							<div className="col_box">
								<div className="icon_box">
									<img
										src="images/check-clock.webp"
										className="profile-icn"
									/>
								</div>
								<div className="text_box">
									<h2>FASTEST PAYOUTS</h2>
									<p>
										Winnings automatically airdropped to
										wallet with no fee
									</p>
								</div>
							</div>
						</div>

						<div className="col-md-4 col-sm-12 col-xl-4 col-12">
							<div className="col_box">
								<div className="icon_box">
									<img
										src="images/gift.webp"
										className="profile-icn"
									/>
								</div>
								<div className="text_box">
									<h2>BET WITH STABLE CRYPTO</h2>
									<p>
										Never worry about the volatility of
										crypto when beting
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			{/* //3 columns section */}

			{/* slider section*/}
			<div className="container-fluid p-0 ">
				<div
					id="carouselExampleCaptions"
					className="carousel slide"
					data-ride="carousel"
				>
					{/* <div className="carousel-inner">
              <div className="carousel-item active">
                  <img src={ (admin_cms_by_page_home) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home[0]?.cms_image}`):('/images/bg.webp')} className="d-block w-100 my-home-page-carousel-img " alt="..."/>
                  <div className="carousel-caption heading-text ">
                      <div className="container-fluid">
                          <div className="container slider-item  justify-content-center">
                              <h2 className="white_h2 text-center heading-1">LIVE</h2>
                              <h2 className="colored_h2 text-center heading-2">BET</h2>
                              <Link href="/dashboard"><a className="slider_btn heading-btn">Play Now</a></Link>
                          </div>
                      </div>
                  </div>
              </div>
              <div className="carousel-item">
                  <img src={ (admin_cms_by_page_home) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home[1]?.cms_image}`):('/images/runway.webp')} className="d-block w-100 my-home-page-carousel-img" alt="..."/>
                  <div className="carousel-caption  heading-text">
                      <div className="container-fluid">
                          <div className="container second-item">
                              <h2 className="white_h2 text-center second-item-heading-text ">Exchange</h2>
                              <h2 className="colored_h2 text-center second-item-heading-text-2">TOKEN</h2>
                              <Link href="/dashboard"><a className="slider_btn heading-btn">Play Now</a></Link>
                          </div>
                      </div>
                  </div>
              </div>

   <div className="carousel-item">
                  <img src={ (admin_cms_by_page_home) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home[2]?.cms_image}`):('/images/bg.webp')} className="d-block w-100 my-home-page-carousel-img" alt="..."/>
                  <div className="carousel-caption  heading-text">
                  <div className="container-fluid">
                  <div className="container second-item">
                  <h2 className="white_h2 text-center heading-1">LIVE</h2>
                  <h2 className="colored_h2 text-center heading-2">BET</h2>
                  <Link href="/dashboard"><a className="slider_btn heading-btn">Play Now</a></Link>
                  </div>
              </div>
                  </div>
   </div>
            <div className="carousel-item">
              <img src={ (admin_cms_by_page_home) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home[3]?.cms_image}`):('/images/earth.webp')} className="d-block w-100 my-home-page-carousel-img" alt="..."/>
                 <div className="carousel-caption  heading-text">
                 <div className="container-fluid">
                 <div className="container second-item">
                 <div className="content">
                            <div className="content-left">
                                <h2 className="white_h2 Powered ">Powered by</h2>
                                <h2 className="colored_h2 Solana  ">Solana BlockChain</h2>
                                <a href="#" className=" Technology-btn">Technology</a>
                            </div>
                            <div className="content-right">
                                <div className="row ">
                                    <div className="logo-group">
                                        <img src="images/logo-group.svg" alt=""/>

                                        <p className="m-3 text-white"> <span>
											<i className="fa-regular fa-circle-right align-items-center text-white"></i>
										</span> Bet Anytime Anywhere</p>

                                        <p className="m-3 text-white"> <span>
											<i className="fa-regular fa-circle-right align-items-center text-white"></i>
										</span>Instant Transactions </p>

                                        <p className="m-3 text-white"> <span>
											<i className="fa-regular fa-circle-right align-items-center text-white"></i>
										</span>Integrated Wallet</p>

                                        <p className="m-3 text-white"> <span>
											<i className="fa-regular fa-circle-right align-items-center text-white"></i>
										</span>~0 Gas Fee</p>
                                    </div>


                                </div>
                            </div>
                        </div>
                  </div>
                  </div>
                  </div>
                  </div>
          </div> */}
					<div className="carousel-inner">
						{admin_cms_by_page_home &&
							admin_cms_by_page_home.map(
								(
									admin_cms_by_page_home_row_v,
									admin_cms_by_page_home_row_i
								) => (
									<div
										key={admin_cms_by_page_home_row_i}
										className={
											admin_cms_by_page_home_row_i == 0
												? "carousel-item active"
												: "carousel-item"
										}
									>
										<img
											src={`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home_row_v?.cms_image}`}
											className="d-block w-100 my-home-page-carousel-img "
											alt="..."
										/>
										<div
											className=""
											dangerouslySetInnerHTML={{
												__html: admin_cms_by_page_home_row_v.cms_desc,
											}}
										/>
										{/* <div className="carousel-caption heading-text">
                          <div className="container-fluid">
                              <div className="container slider-item  justify-content-center">
                                  <h2 className="white_h2 text-center heading-1">LIVE</h2>
                                  <h2 className="colored_h2 text-center heading-2">BET</h2>
                                  <Link href="/dashboard"><a className="slider_btn heading-btn">Play Now</a></Link>
                              </div>
                          </div>
                      </div> */}
									</div>
								)
							)}

						{/* <div className="carousel-item">
                  <img src={ (admin_cms_by_page_home) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home[1]?.cms_image}`):('/images/runway.webp')} className="d-block w-100 my-home-page-carousel-img" alt="..."/>
                  <div className="carousel-caption  heading-text">
                      <div className="container-fluid">
                          <div className="container second-item">
                              <h2 className="white_h2 text-center second-item-heading-text ">Exchange</h2>
                              <h2 className="colored_h2 text-center second-item-heading-text-2">TOKEN</h2>
                              <Link href="/dashboard"><a className="slider_btn heading-btn">Play Now</a></Link>
                          </div>
                      </div>
                  </div>
              </div>

              <div className="carousel-item">
                  <img src={ (admin_cms_by_page_home) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home[2]?.cms_image}`):('/images/bg.webp')} className="d-block w-100 my-home-page-carousel-img" alt="..."/>
                  <div className="carousel-caption  heading-text">
                      <div className="container-fluid">
                          <div className="container second-item">
                              <h2 className="white_h2 text-center heading-1">LIVE</h2>
                              <h2 className="colored_h2 text-center heading-2">BET</h2>
                              <Link href="/dashboard"><a className="slider_btn heading-btn">Play Now</a></Link>
                          </div>
                      </div>
                  </div>
              </div>
              <div className="carousel-item">
                 <img src={ (admin_cms_by_page_home) !== null ? (`${process.env.NEXT_PUBLIC_FILE_PATH}cms_image/${admin_cms_by_page_home[3]?.cms_image}`):('/images/earth.webp')} className="d-block w-100 my-home-page-carousel-img" alt="..."/>
                 <div className="carousel-caption  heading-text">
                    <div className="container-fluid">
                        <div className="container second-item">
                            <div className="content">
                                  <div className="content-left">
                                      <h2 className="white_h2 Powered ">Powered by</h2>
                                      <h2 className="colored_h2 Solana  ">Solana BlockChain</h2>
                                      <a href="#" className=" Technology-btn">Technology</a>
                                  </div>
                                  <div className="content-right">
                                      <div className="row ">
                                          <div className="logo-group">
                                              <img src="images/logo-group.svg" alt=""/>

                                              <p className="m-3 text-white"> <span>
                                                <i className="fa-regular fa-circle-right align-items-center text-white"></i>
                                              </span> Bet Anytime Anywhere</p>

                                                                  <p className="m-3 text-white"> <span>
                                                <i className="fa-regular fa-circle-right align-items-center text-white"></i>
                                              </span>Instant Transactions </p>

                                                                  <p className="m-3 text-white"> <span>
                                                <i className="fa-regular fa-circle-right align-items-center text-white"></i>
                                              </span>Integrated Wallet</p>

                                                                  <p className="m-3 text-white"> <span>
                                                <i className="fa-regular fa-circle-right align-items-center text-white"></i>
                                              </span>~0 Gas Fee</p>
                                          </div>
                                      </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                 </div>
              </div> */}
					</div>
					<button
						className="carousel-control-prev my-bet-left-btn"
						type="button"
						data-target="#carouselExampleCaptions"
						data-slide="prev"
					>
						<i
							className="fa-solid fa-circle-left"
							aria-hidden="true"
						></i>
					</button>
					<button
						className="carousel-control-next my-bet-right-btn"
						type="button"
						data-target="#carouselExampleCaptions"
						data-slide="next"
					>
						<i
							className="fa-solid fa-circle-right"
							aria-hidden="true"
						></i>
					</button>
				</div>
			</div>

			{/*//slider section */}

      {/* category section */}
      <section className="category_sec">
        <div className="container-fluid">
          <div className="row custom_cat_row">
            {game_type && game_type.map((game_type_row_v, game_type_row_i)=>(
              <div key={game_type_row_i} className="col-md-6 col-12">
                <div className="category_col">
                  <div className="cat_img_box">
                    <img src={`${process.env.NEXT_PUBLIC_FILE_PATH}games_type/${game_type_row_v.img}`} />
                  </div>
                  <div className="cat_nameBtn_box">
                    <h3>{game_type_row_v.game_type}</h3>
                    <Link href="/game_type/[Id]" as={`/game_type/${Encryption.encrypt_decrypt('encrypt',game_type_row_v.id.toString())}`} >
                    <a className="cat_btn">
                      BET NOW
                    </a>
                    </Link>
                  </div>
                </div>
              </div>
            ))}
					</div>
				</div>
			</section>
			{/* //category section */}

			{/* featured events section */}
			<section className="featured_events_sec">
				<div className="container-fluid">
					<div className="featured_events_hd">
						<h2>Coming Soon</h2>
					</div>
				</div>
			</section>
			{/* //featured events section */}

			{/* join free section */}
			{op_user && op_user.length == 0 ? (
				<section className="joinfree_sec">
					<div className="container-fluid">
						<div className="text_div">
							<h2>What are you waiting for ?</h2>
							<Link href="/register">
								<a className="join_free_btn">JOIN FREE</a>
							</Link>
							<h4>
								Open and fund your OpSportsbook account today
								and start winning.
							</h4>
						</div>
					</div>
				</section>
			) : (
				""
			)}
			{/* //join free section */}

			{/* How to Get Started section*/}
			<section className="get_started_sec">
				<div className="container-fluid">
					<div className="get_started_hd">
						<h2>How to Get Started ?</h2>
					</div>

					<div className="row get_started_row">
						<div className="col-md-7">
							<div className="start_col">
								<ul className="start_ul">
									<li>
										<i className="fa-solid fa-circle"></i>
										<span>
											Create{" "}
											<a
												href="https://phantom.app/"
												target="_blank"
											>
												Phantom Wallet
											</a>{" "}
											and visit our website.
										</span>
									</li>
									<li>
										<i className="fa-solid fa-circle"></i>
										<span>
											Connect your wallet with simple
											click.
										</span>
									</li>
									<li>
										<i className="fa-solid fa-circle"></i>
										<span>Swap Solana for $BetCoin.</span>
									</li>
									<li>
										<i className="fa-solid fa-circle"></i>
										<span>
											Find the event and outcome you want
											to bet on.
										</span>
									</li>
									<li>
										<i className="fa-solid fa-circle"></i>
										<span>
											Place the bet to secure your odds,
											Winnings are air-dropped directly to
											your wallet.
										</span>
									</li>
								</ul>
							</div>
						</div>
						<div className="col-md-5">
							<div className="start_col right_img_col">
								<div className="start_col right_img_col_inner">
									<img
										src="images/viedio-thumbnail.svg"
										className="img-fluid"
									/>
								</div>
									<a className="quick_tutorial_btn" href="https://www.youtube.com/watch?v=jGIUlMwTOdc&t=8s" target="_blank">
										Quick Tutorial
									</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			{/* //How to Get Started section */}

			{/* gambling problem section */}
			<section className="gamble_sec">
				<div className="container-fluid">
					<div className="gamble_hd">
						<h2>
							<a href="https://www.ncpgambling.org/help-treatment/national-helpline-1-800-522-4700/" target="_blank" className="text-white">
								If you have a Gambling Problem?
							</a>
						</h2>
					</div>
				</div>
			</section>
			<Footer />
		</>
	);
}
export default homepage;
