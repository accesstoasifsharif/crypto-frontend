import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import Footer from "../components/layout/Footer";
import Header from "../components/layout/Header";
import Navbar from "../components/layout/Navbar";
import MyWallet from "../components/wallet/MyWallet";

import axios from "axios";
import rateLimit from "axios-rate-limit";
import $ from "jquery";
import Swal from "sweetalert2";
import UserAuth from "../user_auth";
let UserCheckAuthObj = new UserAuth();



function user_profile() {
	const router = useRouter();
	const [op_user, set_op_user] = useState(null);
	const [bets_upcoming, set_bets_upcoming] = useState(null);
	const [bets_upcoming_p, set_bets_upcoming_p] = useState([]);
	// const [bets_started, set_bets_started] = useState(null);
	// const [bets_started_p, set_bets_started_p] = useState([]);
	const [bets_completed, set_bets_completed] = useState(null);
	const [bets_completed_p, set_bets_completed_p] = useState([]);

	// const [games_upcoming, set_games_upcoming] = useState(null);
	// const [games_upcoming_p, set_games_upcoming_p] = useState([]);

	// const [total_investment_earned, set_total_investment_earned] = useState(null);
	const [total_earned, set_total_earned] = useState(null);
	const [total_investment, set_total_investment] = useState(null);


	const [refreshToken, setRefreshToken] = useState(Math.random());
	useEffect(() => {
		if (!router.isReady) return;

		UserCheckAuthObj.UserCheckAuth({ router });
		var user_from_local_storage = getUserFromLocalStorage();
		getUserStats(user_from_local_storage.remember_token);

		getActiveBets(user_from_local_storage.remember_token);
		getHistoryBets(user_from_local_storage.remember_token);
		//set_op_user(user_from_local_storage);

		const myResponse = [];
		if ( Object.keys(user_from_local_storage).length !== 0 && user_from_local_storage.constructor === Object ) {
			retrieveBetData( user_from_local_storage.id, user_from_local_storage.remember_token )
			.then(
			axios.spread((...response) => {
				for (let i = 0; i <= 3; i += 1) {
					myResponse.push(response[i].data);
				}
				//console.log(response)
				if (myResponse[0].results.length != 0) {
					// set_bets_upcoming(myResponse[0].results);
					// set_bets_upcoming_p(myResponse[0].results);
					// const data = { req_per_page: document.getElementById( "bets_upcoming_req_per_page" ).value, page_no: 1, };
					// bets_upcoming_pagination( data, myResponse[0].results );
				}
				//if(myResponse[1].results.length != 0) set_bets_started(myResponse[1].results);
				// if(myResponse[1].results.length != 0){
				//     set_bets_started(myResponse[1].results);
				//     set_bets_started_p(myResponse[1].results);
				//     const data = { "req_per_page": document.getElementById("bets_started_req_per_page").value, "page_no": 1 };
				//     bets_started_pagination(data, myResponse[1].results);
				// }
				if (myResponse[1].results.length != 0) { 
					// set_bets_completed(myResponse[1].results);
					// set_bets_completed_p(myResponse[1].results);
					// const data = { "req_per_page": document.getElementById( "bets_completed_req_per_page" ).value, page_no: 1, };
					// bets_completed_pagination( data, myResponse[1].results );
				}
				// if(myResponse[3].results.length != 0){
				//     set_games_upcoming(myResponse[3].results);
				//     set_games_upcoming_p(myResponse[3].results);
				//     const data = { "req_per_page": document.getElementById("req_per_page").value, "page_no": 1 };
				//     pagination(data, myResponse[3].results);
				// }
				localStorage.setItem( "op_user", JSON.stringify(myResponse[2].user) );
				set_op_user(myResponse[2].user);
				// set_total_investment_earned(myResponse[3].results);
				//console.log(jsonBetsArrayFilterByTimeline(myResponse[0].results,"0"));
				// set_res_tableList(myResponse[0].results);
				// if(myResponse[0].results.length != 0 && res_tableList === null){
				//     set_table_detailsById(jsonArrayFilterForNewOrder(myResponse[0].results)[0]);
				//     if((jsonArrayFilterForNewOrder(myResponse[0].results)[0]).lastorderdetail){
				//         set_active_table_order_id((jsonArrayFilterForNewOrder(myResponse[0].results)[0]).lastorderdetail.id)
				//     }
				// }
				// set_res_table(chunkArray(myResponse[0].results,7));
				// set_res_new_order_table(chunkArray(jsonArrayFilterForNewOrder(myResponse[0].results),7));
				// set_res_waiters(myResponse[1].results);
			})
			)
			.finally(() => {
				// Update refreshToken after 3 seconds so this event will re-trigger and update the data
				//setTimeout(() => setRefreshToken(Math.random()), 15000);
			});
		}

	}, [router.isReady, refreshToken]);

	function getUserFromLocalStorage() {
		return JSON.parse(localStorage.getItem("op_user")) || [];
	}
	const jsonBetsArrayFilterByTimeline = (arr, timeline) => {
		return arr.filter(function (arr_v) {
			return arr_v.timeline == timeline;
		});
	};
	async function retrieveBetData(user_id, remember_token) {
		const http = rateLimit(
			axios.create({
				headers: {
					Accept: "application/json",
					Authorization: "Bearer " + remember_token,
				},
			}),
			{ maxRequests: 6, perMilliseconds: 1000, maxRPS: 6 }
		);
		const res = await axios.all([
			http.post(
				`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=0,1`
			),
			//http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=1`),
			http.post(
				`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=2`
			),
			//http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/games?timeline=0`),
			http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile`),
			http.post(
				`${process.env.NEXT_PUBLIC_API_DOMAIN}user/total_investment_earned`
			),
		]);

		return res;
	}

	const paginator = (items, current_page, per_page_items) => {
		//function paginator(items, current_page, per_page_items) {
		//console.log([typeof current_page, typeof per_page_items]);
		let page = current_page || 1,
			per_page = per_page_items || 10,
			offset = (page - 1) * per_page,
			paginatedItems = items.slice(offset).slice(0, per_page_items),
			total_pages = Math.ceil(items.length / per_page);

		return {
			page: page,
			per_page: per_page,
			pre_page: page - 1 ? page - 1 : null,
			next_page: total_pages > page ? page + 1 : null,
			total: items.length,
			total_pages: total_pages,
			data: paginatedItems,
		};
	};

	/* // Pagination logic implementation
    const pagination = (data, all_data) => {
        
        $("#games_upcoming_pagination").empty();
        if (data.req_per_page !== 'ALL') {
            const myarr = paginator(all_data, data.page_no, parseInt(data.req_per_page));
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            let pager = `<a id="prev_link" class="games_upcoming_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
                `<a class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;
            
            for (let num = 2; num <= myarr.total_pages; num++) {
                pager += `<a  class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
            }
            pager += `<a id="next_link"  class="games_upcoming_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;
            
            $("#games_upcoming_pagination").append(pager);
            
            $('div#games_upcoming_pagination a').each(function(){
                if(this.innerHTML == myarr.page){
                    $(this).addClass('active');
                }
            });

            pagination_initialize('games_upcoming_p_btn','req_per_page');
            set_games_upcoming(myarr.data);
            
        } else {
            const myarr = paginator(all_data, data.page_no, all_data.length);
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            set_games_upcoming(myarr.data);
        }
        
    } */

	// Pagination logic implementation
	const bets_completed_pagination = (data, all_data) => {
		$("#bets_completed_pagination").empty();
		if (data.req_per_page !== "ALL") {
			const myarr = paginator(
				all_data,
				data.page_no,
				parseInt(data.req_per_page)
			);
			//console.log(myarr);
			const string_all_data = JSON.stringify(all_data);
			let pager =
				`<a id="prev_link" class="bets_completed_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
				`<a class="bets_completed_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;

			for (let num = 2; num <= myarr.total_pages; num++) {
				pager += `<a  class="bets_completed_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
			}
			pager += `<a id="next_link"  class="bets_completed_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;

			$("#bets_completed_pagination").append(pager);

			$("div#bets_completed_pagination a").each(function () {
				if (this.innerHTML == myarr.page) {
					$(this).addClass("active");
				}
			});

			bets_completed_pagination_initialize(
				"bets_completed_p_btn",
				"bets_completed_req_per_page"
			);
			set_bets_completed(myarr.data);
		} else {
			const myarr = paginator(all_data, data.page_no, all_data.length);
			//console.log(myarr);
			const string_all_data = JSON.stringify(all_data);
			set_bets_completed(myarr.data);
		}
	};

	/* // Pagination logic implementation
    const bets_started_pagination = (data, all_data) => {
        
        $("#bets_started_pagination").empty();
        if (data.req_per_page !== 'ALL') {
            const myarr = paginator(all_data, data.page_no, parseInt(data.req_per_page));
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            let pager = `<a id="prev_link" class="bets_started_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
                `<a class="bets_started_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;
            
            for (let num = 2; num <= myarr.total_pages; num++) {
                pager += `<a  class="bets_started_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
            }
            pager += `<a id="next_link"  class="bets_started_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;
            
            $("#bets_started_pagination").append(pager);
            
            $('div#bets_started_pagination a').each(function(){
                if(this.innerHTML == myarr.page){
                    $(this).addClass('active');
                }
            });

            bets_started_pagination_initialize('bets_started_p_btn','bets_started_req_per_page');
            set_bets_started(myarr.data);
            
        } else {
            const myarr = paginator(all_data, data.page_no, all_data.length);
            //console.log(myarr);
            const string_all_data  = JSON.stringify(all_data);
            set_bets_started(myarr.data);
        }
        
    } */

	// Pagination logic implementation
	const bets_upcoming_pagination = (data, all_data) => {
		$("#bets_upcoming_pagination").empty();
		if (data.req_per_page !== "ALL") {
			const myarr = paginator(
				all_data,
				data.page_no,
				parseInt(data.req_per_page)
			);
			//console.log(myarr);
			const string_all_data = JSON.stringify(all_data);
			let pager =
				`<a id="prev_link" class="bets_upcoming_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
				`<a class="bets_upcoming_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;

			for (let num = 2; num <= myarr.total_pages; num++) {
				pager += `<a  class="bets_upcoming_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
			}
			pager += `<a id="next_link"  class="bets_upcoming_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;

			$("#bets_upcoming_pagination").append(pager);

			$("div#bets_upcoming_pagination a").each(function () {
				if (this.innerHTML == myarr.page) {
					$(this).addClass("active");
				}
			});

			bets_upcoming_pagination_initialize(
				"bets_upcoming_p_btn",
				"bets_upcoming_req_per_page"
			);
			set_bets_upcoming(myarr.data);
		} else {
			const myarr = paginator(all_data, data.page_no, all_data.length);
			//console.log(myarr);
			const string_all_data = JSON.stringify(all_data);
			set_bets_upcoming(myarr.data);
		}
	};

	/* // trigger when requests per page dropdown changes
    const pagination_initialize = (pagination_btn,req_per_page_select) => {
        document.querySelectorAll('.'+pagination_btn).forEach(btn => {
            btn.addEventListener('click', () => {
                const page_no = btn.getAttribute("data-id");
                if(page_no !== 'null'){
                    const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": parseInt(page_no) };
                    pagination(data, JSON.parse(btn.getAttribute("data-value")));
                }
                
            });
        });
    }
    const filter_requests = async (games_upcoming_p,req_per_page_select) => {
        const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": 1 };
        pagination(data, games_upcoming_p);
    } */

	const bets_completed_pagination_initialize = (
		pagination_btn,
		req_per_page_select
	) => {
		document.querySelectorAll("." + pagination_btn).forEach((btn) => {
			btn.addEventListener("click", () => {
				const page_no = btn.getAttribute("data-id");
				if (page_no !== "null") {
					const data = {
						req_per_page:
							document.getElementById(req_per_page_select).value,
						page_no: parseInt(page_no),
					};
					bets_completed_pagination(
						data,
						JSON.parse(btn.getAttribute("data-value"))
					);
				}
			});
		});
	};
	const bets_completed_filter_requests = async (
		bets_completed_p,
		req_per_page_select
	) => {
		const data = {
			req_per_page: document.getElementById(req_per_page_select).value,
			page_no: 1,
		};
		//console.log([bets_completed_p, req_per_page_select]);
		bets_completed_pagination(data, bets_completed_p);
	};

	/* const bets_started_pagination_initialize = (pagination_btn,req_per_page_select) => {
        document.querySelectorAll('.'+pagination_btn).forEach(btn => {
            btn.addEventListener('click', () => {
                const page_no = btn.getAttribute("data-id");
                if(page_no !== 'null'){
                    const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": parseInt(page_no) };
                    bets_started_pagination(data, JSON.parse(btn.getAttribute("data-value")));
                }
                
            });
        });
    }
    const bets_started_filter_requests = async (bets_started_p,req_per_page_select) => {
        const data = { "req_per_page": document.getElementById(req_per_page_select).value, "page_no": 1 };
        //console.log([bets_completed_p, req_per_page_select]);
        bets_started_pagination(data, bets_started_p);
    } */

	const bets_upcoming_pagination_initialize = (
		pagination_btn,
		req_per_page_select
	) => {
		document.querySelectorAll("." + pagination_btn).forEach((btn) => {
			btn.addEventListener("click", () => {
				const page_no = btn.getAttribute("data-id");
				if (page_no !== "null") {
					const data = {
						req_per_page:
							document.getElementById(req_per_page_select).value,
						page_no: parseInt(page_no),
					};
					bets_upcoming_pagination(
						data,
						JSON.parse(btn.getAttribute("data-value"))
					);
				}
			});
		});
	};
	const bets_upcoming_filter_requests = async (
		bets_upcoming_p,
		req_per_page_select
	) => {
		const data = {
			req_per_page: document.getElementById(req_per_page_select).value,
			page_no: 1,
		};
		//console.log([bets_completed_p, req_per_page_select]);
		bets_upcoming_pagination(data, bets_upcoming_p);
	};

	const bet_now = async (upcoming_games_row_v) => {
		router.push("/games");
		// console.log(upcoming_games_row_v);
		// $('#bet_team').empty();
		// $('#bet_team').append($('<option>').text(upcoming_games_row_v.team1detail.name).attr('value', 1));
		// $('#bet_team').append($('<option>').text(upcoming_games_row_v.team2detail.name).attr('value', 2));
		// $("#game_id").val(upcoming_games_row_v.id);
		// $("#user_id").val(op_user.id);
		// $('#betNow_modal').modal('toggle');
	};
	const bet_now_submit = async (event) => {
		event.preventDefault();
		console.log([
			event.target.user_id.value,
			event.target.game_id.value,
			event.target.bet_amount.value,
			event.target.bet_team.value,
		]);

		var alert_message = "Please Fill These Fields: <br>";
		var newLine = "\r\n";
		alert_message += newLine;

		if (
			event.target.user_id.value == "" ||
			event.target.game_id.value == "" ||
			event.target.bet_amount.value == "" ||
			event.target.bet_team.value == ""
		) {
			if (event.target.user_id.value == "") {
				alert_message += "User Id can not be left blank. <br>";
				alert_message += newLine;
			}
			if (event.target.game_id.value == "") {
				alert_message += "Game Id can not be left blank. <br>";
				alert_message += newLine;
			}
			if (event.target.bet_amount.value == "") {
				alert_message += "Amount can not be left blank. <br>";
				alert_message += newLine;
			}
			if (event.target.bet_team.value == "") {
				alert_message += "Team can not be left blank. <br>";
				alert_message += newLine;
			}

			Swal.fire({
				icon: "error",
				title: "Oops...",
				//text: alert_message
				html: alert_message,
				//footer: '<a href="">Why do I have this issue?</a>'
			});
			return false;
		}

		var data = new Object();
		data.user_id = event.target.user_id.value;
		data.game_id = event.target.game_id.value;
		data.bet_amount = event.target.bet_amount.value;
		data.bet_team = event.target.bet_team.value;
		var config = {
			method: "post",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets/add`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + op_user.remember_token,
			},
			data: data,
		};

		axios(config)
			.then(function (response) {
				if (response.data.hasOwnProperty("message")) {
					if (response.data.message == "Authenticated") {
						//console.log(response.data);
						if (response.data.message_title == "success") {
							set_bets_upcoming(response.data.results);
							set_bets_upcoming_p(response.data.results);
							const data = {
								req_per_page: document.getElementById(
									"bets_upcoming_req_per_page"
								).value,
								page_no: 1,
							};
							bets_upcoming_pagination(
								data,
								response.data.results
							);
							swalAutoclose(
								"success",
								"Bet Placed Successfully",
								1000
							);
						} else {
							Swal.fire("Unable to bet " + response.data.results);
						}
					} else {
						swalAutoclose("error", "Unable to bet", 1000);
					}
				} else {
					swalAutoclose("error", "Unable to bet", 1000);
				}
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to bet", 1000);
			});
	};
	const swalAutoclose = (title, html, timer, timer_progress_bar = false) => {
		let timerInterval;
		return Swal.fire({
			title: title,
			html: html,
			timer: timer,
			timerProgressBar: timer_progress_bar,
			didOpen: () => {
				Swal.showLoading();
				//const b = Swal.getHtmlContainer().querySelector('b')
				// timerInterval = setInterval(() => {
				// b.textContent = Swal.getTimerLeft()
				// }, 100)
			},
			willClose: () => {
				clearInterval(timerInterval);
			},
		}).then((result) => {
			/* Read more about handling dismissals below */
			if (result.dismiss === Swal.DismissReason.timer) {
				//console.log('I was closed by the timer')
			}
		});
	};

	const change_password_submit = async (event) => {
		event.preventDefault();
		console.log([
			event.target.password.value,
			event.target.password_c.value,
			event.target.password_o.value,
		]);

		var alert_message = "";
		var newLine = "\r\n";
		alert_message += newLine;
		if (event.target.password.value !== event.target.password_c.value) {
			alert_message +=
				"New Password & Confirm new password didn't match. <br>";
			alert_message += newLine;
			Swal.fire({
				icon: "error",
				title: "Oops...",
				//text: alert_message
				html: alert_message,
				//footer: '<a href="">Why do I have this issue?</a>'
			});
			return false;
		}

		if (
			event.target.password.value == "" ||
			event.target.password_c.value == "" ||
			event.target.password_o.value == ""
		) {
			alert_message = "Please Fill These Fields: <br>";
			var newLine = "\r\n";
			alert_message += newLine;
			if (event.target.password.value !== event.target.password_c.value) {
				alert_message +=
					"New Password & Confirm new password didn't match. <br>";
				alert_message += newLine;
				Swal.fire({
					icon: "error",
					title: "Oops...",
					//text: alert_message
					html: alert_message,
					//footer: '<a href="">Why do I have this issue?</a>'
				});
				return false;
			}
			if (event.target.password.value == "") {
				alert_message += "New password can not be left blank. <br>";
				alert_message += newLine;
			}
			if (event.target.password_c.value == "") {
				alert_message +=
					"Confirm new password can not be left blank. <br>";
				alert_message += newLine;
			}
			if (event.target.password_o.value == "") {
				alert_message += "Old password can not be left blank. <br>";
				alert_message += newLine;
			}

			Swal.fire({
				icon: "error",
				title: "Oops...",
				//text: alert_message
				html: alert_message,
				//footer: '<a href="">Why do I have this issue?</a>'
			});
			return false;
		}

		var data = new Object();
		data.password = event.target.password.value;
		data.password_confirmation = event.target.password_c.value;
		data.password_old = event.target.password_o.value;
		var config = {
			method: "post",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/change_password`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + op_user.remember_token,
			},
			data: data,
		};

		axios(config)
			.then(function (response) {
				if (response.data.hasOwnProperty("message")) {
					if (response.data.message == "Authenticated") {
						//console.log(response.data);
						if (
							response.data.message_title ==
							"Password changed successfully"
						) {
							localStorage.setItem(
								"op_user",
								JSON.stringify(response.data.user)
							);
							set_op_user(response.data.user);
							swalAutoclose(
								"success",
								"Password changed successfully",
								1000
							);
						} else {
							Swal.fire("Unable to change password");
						}
					} else {
						swalAutoclose(
							"error",
							"Unable to change password",
							1000
						);
					}
				} else {
					swalAutoclose("error", "Unable to change password", 1000);
				}
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to change password", 1000);
			});
	};

	const load_file = async (event) => {
		var image = document.getElementById("output");
		//console.log(event.target.files);
		if (event.target.files.length != 0) {
			image.src = URL.createObjectURL(event.target.files[0]);

			// var data = new Object();
			// data.image = event.target.files[0];
			const data = new FormData();
			data.append("image", event.target.files[0]);
			data.append("name", op_user.name);
			var config = {
				method: "post",
				url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile_update`,
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					Authorization: "Bearer " + op_user.remember_token,
				},
				data: data,
			};

			axios(config)
				.then(function (response) {
					if (response.data.hasOwnProperty("message")) {
						if (response.data.message == "Authenticated") {
							//console.log(response.data);
							if (
								response.data.message_title ==
								"User profile updated successfully"
							) {
								localStorage.setItem(
									"op_user",
									JSON.stringify(response.data.user)
								);
								set_op_user(response.data.user);
								swalAutoclose(
									"success",
									"Profile pic updated successfully",
									1000
								);
							} else {
								Swal.fire("Unable to update profile pic");
							}
						} else {
							swalAutoclose(
								"error",
								"Unable to update profile pic",
								1000
							);
						}
					} else {
						swalAutoclose(
							"error",
							"Unable to update profile pic",
							1000
						);
					}
				})
				.catch(function (error) {
					swalAutoclose(
						"error",
						"Unable to update profile pic",
						1000
					);
				});
		}
	};

	async function getUserStats(remember_token) {
		var config = {
			method: "get",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/get_user_stats`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + remember_token,
			},
		};
		await axios(config)
			.then(function (response) {
				set_total_investment(response.data.total_investment);
				set_total_earned(response.data.total_earned);
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to fetch betc purchased.", 1000);
			});
	}

	async function getActiveBets(remember_token) {
		var config = {
			method: "get",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/getBetsData?status=0`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + remember_token,
			},
		};
		await axios(config)
			.then(function (response) {
				console.log('getActiveBets');
				console.log(response.data);
				set_bets_upcoming(response.data);
				set_bets_upcoming_p(response.data);
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to fetch betc purchased.", 1000);
			});
	}
	async function getHistoryBets(remember_token) {
		var config = {
			method: "get",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/getBetsData?status=1,2,3`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + remember_token,
			},
		};
		await axios(config)
			.then(function (response) {
				console.log('getHistoryBets');
				console.log(response.data);
				set_bets_completed(response.data);
				set_bets_completed_p(response.data);
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to fetch betc purchased.", 1000);
			});
	}

	//console.log([bets_upcoming, bets_started, bets_completed, games_upcoming, process.env.NEXT_PUBLIC_BACK_DOMAIN, total_investment_earned]);
	//console.log(bets_started);
	return (
		<>
			<style global jsx>{``}</style>
			<Header /* data={op_user} */ />
			<Navbar /* data={op_user} */ />

			{/* Header start */}
			<section>
				<div className="container-fluid profile-bg">
					<div className="row">
						<div className="col-md-3  text-center py-3 profile-section">
							{/* <i className="fa fa-user-circle profile-icn" aria-hidden="true"></i> */}
							<div className="profile-pic">
								<label className="-label" htmlFor="file">
									<span className="fa-solid fa-camera-retro "></span>
									<span>Change Image</span>
								</label>
								<input
									id="file"
									type="file"
									onChange={() => load_file(event)}
								/>
								<img
									src={
										op_user?.image !== null
											? `${process.env.NEXT_PUBLIC_FILE_PATH}Users_profile_photo/${op_user?.image}`
											: "/images/profile_1584621964.png"
									}
									id="output"
									width="200"
								/>
							</div>
							<h3 className="profile-name">
								{op_user && op_user.name}
							</h3>
							<p className="profile-id">
								{" "}
								ID # {op_user && op_user.id}
							</p>
						</div>
						<div className="col-md-7">
							<div className="row mt-5 mb-3">
								<div className="col-md-6">
									<span className="investment-icon">
										<i className="fa-solid fa-sack-dollar"></i>
									</span>
									<h3 className="investment">
										Total investment
									</h3>
									<span className="amount-investement">
										${" "}
										{total_investment &&
											total_investment.toFixed(2)}
									</span>
								</div>
								<div className="col-md-6">
									<span className="investment-icon">
										<i className="fa-solid fa-hand-holding-dollar"></i>
									</span>
									<h3 className="investment">Total Earned</h3>
									<span className="amount-investement">
										${" "}
										{total_earned &&
											total_earned.toFixed(2)}
									</span>
								</div>
							</div>
						</div>
						<div className="col-md-2 my-wallet text-center">
							<MyWallet></MyWallet>
							<p className="change-password-button mt-3">
								<a
									href="#"
									className="change-password-btn"
									data-toggle="modal"
									data-target="#change_password_modal"
								>
									Change password
								</a>
							</p>
						</div>
					</div>
				</div>

				<div className="container-fluid  profile-section-bg">
					<div className="row">
						<div className="col-md-6  user-profile-section text-center py-3 col-6">
							<p className="user-profile-section-activites">
								<Link href="/dashboard">Recent Activity</Link>
							</p>
						</div>
						<div className="col-md-6 text-center py-3 user-profile-section col-6">
							<p className="user-profile-section-bank-roles">
								{" "}
								<Link href="/transactions">Transactions</Link>
							</p>
						</div>
					</div>

					<div className="container pb-5">
						<div className="row">
							<div className="col-md-12 text-center">
								<p className="History-Bets">
									Current Active Bets
								</p>
							</div>
						</div>
						<div className="row History-Bets-data-bg">
							<div className="col-md-12">
								<div className="row History-Bets-bg pt-3">
									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-desc">
											Game Type
										</p>
									</div>
									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-price">
											Team1
										</p>
									</div>
									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-type">
											Team2
										</p>
									</div>
									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-time">
											Your Team
										</p>
									</div>
									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-end-time">
											Amount
										</p>
									</div>

									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-status">
											Venue
										</p>
									</div>
								</div>
							</div>
							<div className="col-md-12">
								{bets_upcoming &&
									bets_upcoming.map(
										(
											bets_upcoming_row_v,
											bets_upcoming_row_i
										) => (
											<div
												key={bets_upcoming_row_i}
												className="row"
											>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="bet-desc">
														{
															bets_upcoming_row_v
																.games
																.game_type_name
														}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="bet-price">
														{
															bets_upcoming_row_v
																.team1detail
																.name
														}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="bet-type">
														{
															bets_upcoming_row_v
																.team2detail
																.name
														}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="bet-time">
														{
															bets_upcoming_row_v
																.betteamdetail
																.name
														}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="bet-end-price">
														$
														{bets_upcoming_row_v.bet_amount.toFixed(
															2
														)}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="status">
														{
															bets_upcoming_row_v
																.games.venue
														}
													</p>
												</div>
											</div>
										)
									)}
								<div className="paginator_btn">
									<div className="clearfix">
										<div className="box options">
											<label>Requests Per Page: </label>
											<select
												id="bets_upcoming_req_per_page"
												onChange={() =>
													bets_upcoming_filter_requests(
														bets_upcoming_p,
														"bets_upcoming_req_per_page"
													)
												}
											>
												<option>5</option>
												<option>1</option>
												<option>10</option>
												<option>ALL</option>
											</select>
										</div>
										<div
											id="bets_upcoming_pagination"
											className="box pagination"
										></div>
									</div>
								</div>

								{bets_upcoming == null ? (
									<div className="text-center pagination_no_record">
										No Record
									</div>
								) : (
									""
								)}
							</div>
						</div>

						<div className="row">
							<div className="col-md-12 text-center">
								<p className="History-Bets">History Bets</p>
							</div>
						</div>
						<div className="row History-Bets-data-bg">
							<div className="col-md-12">
								<div className="row History-Bets-bg pt-3">
									<div className="col-xl-1 col-1 col-sm-1">
										<p className="History-Bets-desc">
											Game Type
										</p>
									</div>
									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-price">
											Team1
										</p>
									</div>
									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-type">
											Team2
										</p>
									</div>
									<div className="col-xl-1 col-1 col-sm-1">
										<p className="History-Bets-time">
											Your Team
										</p>
									</div>

									<div className="col-xl-2 col-2 col-sm-2">
										<p className="History-Bets-end-time">
											Bet On
										</p>
									</div>

									<div className="col-xl-1 col-1 col-sm-1">
										<p className="History-Bets-end-time">
											Amount
										</p>
									</div>

									<div className="col-xl-1 col-1 col-sm-1">
										<p className="History-Bets-status">
											Venue
										</p>
									</div>
									<div className="col-xl-1 col-1 col-sm-1">
										<p className="History-Bets-end-time">
											Won Team
										</p>
									</div>
									<div className="col-xl-1 col-1 col-sm-1">
										<p className="History-Bets-status">
											Status
										</p>
									</div>
								</div>
							</div>
							<div className="col-md-12">
								{bets_completed &&
									bets_completed.map(
										(
											bets_completed_row_v,
											bets_completed_row_i
										) => (
											<div
												key={bets_completed_row_i}
												className="row"
											>
												<div className="col-xl-1 col-1 col-sm-1">
													<p className="bet-desc">
														{
															bets_completed_row_v
																.games
																.game_type_name
														}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="bet-price">
														{
															bets_completed_row_v
																.team1detail
																.name
														}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2">
													<p className="bet-type">
														{
															bets_completed_row_v
																.team2detail
																.name
														}
													</p>
												</div>
												<div className="col-xl-1 col-1 col-sm-1">
													<p className="bet-time">
														{
															bets_completed_row_v
																.betteamdetail
																.name
														}
													</p>
												</div>
												<div className="col-xl-2 col-2 col-sm-2" dangerouslySetInnerHTML={{__html: bets_completed_row_v.bet_on}}>
												</div>
												<div className="col-xl-1 col-1 col-sm-1">
													<p className="bet-end-price">
														$
														{bets_completed_row_v.bet_amount.toFixed(
															2
														)}
													</p>
												</div>
												<div className="col-xl-1 col-1 col-sm-1">
													<p className="status">
														{
															bets_completed_row_v
																.games.venue
														}
													</p>
												</div>
												<div className="col-xl-1 col-1 col-sm-1">
													<p className="status">
														{bets_completed_row_v
															.games.result == "0"
															? "No Result"
															: bets_completed_row_v
																	.games
																	.result ==
															  "1"
															? "Team1"
															: bets_completed_row_v
																	.games
																	.result ==
															  "2"
															? "Team2"
															: "Draw"}
													</p>
												</div>
												<div className="col-xl-1 col-1 col-sm-1">
													<p className="status">
														{bets_completed_row_v.bet_team ==
														bets_completed_row_v
															.games.result
															? "Won"
															: /* : bets_completed_row_v.games.result=='1' ? 'Team1' */
															  "Lost"}
													</p>
												</div>
											</div>
										)
									)}
								<div className="paginator_btn">
									<div className="clearfix">
										<div className="box options">
											<label>Requests Per Page: </label>
											<select
												id="bets_completed_req_per_page"
												onChange={() =>
													bets_completed_filter_requests(
														bets_completed_p,
														"bets_completed_req_per_page"
													)
												}
											>
												<option>5</option>
												<option>1</option>
												<option>10</option>
												<option>ALL</option>
											</select>
										</div>
										<div
											id="bets_completed_pagination"
											className="box pagination"
										></div>
									</div>
								</div>
           					</div>
       					</div>
   					</div>
				</div>

				{/* <!-- betting model --> */}
				<div
					className="modal fade betNowModal"
					id="betNow_modal"
					tabIndex="-1"
					role="dialog"
					aria-labelledby="exampleModalCenterTitle"
					aria-hidden="true"
				>
					<div
						className="modal-dialog modal-dialog-centered"
						role="document"
					>
						<div className="modal-content">
							<div className="modal-header">
								<h5
									className="modal-title"
									id="exampleModalLongTitle"
								>
									Bet Now
								</h5>
								<button
									type="button"
									className="close"
									data-dismiss="modal"
									aria-label="Close"
								>
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form onSubmit={bet_now_submit}>
								<div className="modal-body">
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<label htmlFor="">
													Enter Betting Amount ($)
												</label>
												<input
													type="text"
													className="form-control"
													name="bet_amount"
													id="bet_amount"
													placeholder="Enter bet amount"
												/>
												<input
													type="hidden"
													className="form-control"
													name="user_id"
													id="user_id"
												/>
												<input
													type="hidden"
													className="form-control"
													name="game_id"
													id="game_id"
												/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<label htmlFor="exampleFormControlSelect1">
													Bet on team
												</label>
												<select
													className="form-control"
													name="bet_team"
													id="bet_team"
												>
													{/* <option value="1">Team1</option>
                                                  <option value="2">Team2</option> */}
												</select>
											</div>
										</div>
									</div>
								</div>
								<div className="modal-footer">
									<button
										type="submit"
										className="placebet_btn"
									>
										Place Bet
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				{/* <!-- //betting model --> */}

				{/* <!-- change passsword model --> */}
				<div
					className="modal fade betNowModal"
					id="change_password_modal"
					tabIndex="-1"
					role="dialog"
					aria-labelledby="exampleModalCenterTitle"
					aria-hidden="true"
				>
					<div
						className="modal-dialog modal-dialog-centered"
						role="document"
					>
						<div className="modal-content">
							<div className="modal-header">
								<h5
									className="modal-title"
									id="exampleModalLongTitle"
								>
									Change password
								</h5>
								<button
									type="button"
									className="close"
									data-dismiss="modal"
									aria-label="Close"
								>
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form onSubmit={change_password_submit}>
								<div className="modal-body">
									<div className="row">
										<div className="col-md-12">
											<div className="form-group">
												<label htmlFor="password">
													Enter New Password
												</label>
												<input
													type="password"
													className="form-control"
													name="password"
													id="password"
													placeholder="Enter new password"
												/>
											</div>
										</div>
										<div className="col-md-12">
											<div className="form-group">
												<label htmlFor="password_c">
													Confirm New Password
												</label>
												<input
													type="password"
													className="form-control"
													name="password_c"
													id="password_c"
													placeholder="Confirm new password"
												/>
											</div>
										</div>
										<div className="col-md-12">
											<div className="form-group">
												<label htmlFor="password_o">
													Enter Old Password
												</label>
												<input
													type="password"
													className="form-control"
													name="password_o"
													id="password_o"
													placeholder="Enter old password"
												/>
											</div>
										</div>
									</div>
								</div>
								<div className="modal-footer">
									<button
										type="submit"
										className="placebet_btn"
									>
										Save
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				{/* <!-- //change passsword model --> */}
			</section>
			<Footer />
		</>
	);
}
export default user_profile;
