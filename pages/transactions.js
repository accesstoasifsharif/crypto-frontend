import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import Footer from "../components/layout/Footer";
import Header from "../components/layout/Header";
import Navbar from "../components/layout/Navbar";
import MyWallet from "../components/wallet/MyWallet";
import axios from "axios";
import rateLimit from "axios-rate-limit";
import $ from "jquery";
import Swal from "sweetalert2";
import UserAuth from "../user_auth";
let UserCheckAuthObj = new UserAuth();

import { Line } from "react-chartjs-2";
import Chart from "chart.js/auto";

const data = {
	labels: ["January", "February", "March", "April", "May", "June", "July"],
	datasets: [
		{
			label: "Wallet balance",
			fill: false,
			lineTension: 0.1,
			backgroundColor: "rgba(75,192,192,0.4)",
			borderColor: "rgba(75,192,192,1)",
			borderCapStyle: "butt",
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle: "miter",
			pointBorderColor: "rgba(75,192,192,1)",
			pointBackgroundColor: "#fff",
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: "rgba(75,192,192,1)",
			pointHoverBorderColor: "rgba(220,220,220,1)",
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [65, 59, 80, 81, 56, 55, 40],
		},
	],
};

function my_profile() {
	const router = useRouter();
	const [op_user, set_op_user] = useState(null);

	const [bets_completed, set_bets_completed] = useState(null);
	const [bets_completed_p, set_bets_completed_p] = useState([]);

	const [refreshToken, setRefreshToken] = useState(Math.random());

	// Asif start
	const [total_betc_purchased, set_total_betc_purchased] = useState([]);
	const [total_betc_sale, set_total_betc_sale] = useState([]);
	const [all_crypto_transactions, set_all_crypto_transactions] = useState([]);
	const [total_earned, set_total_earned] = useState(null);
	const [total_investment, set_total_investment] = useState(null);
	// const [all_crypto_transactions, set_all_crypto_transactions] = useState([]);
	// Asif end

	useEffect(() => {
		if (!router.isReady) return;
		UserCheckAuthObj.UserCheckAuth({ router });
		var user_from_local_storage = getUserFromLocalStorage();
		set_op_user(user_from_local_storage);

		getTotalBetcPurchased(
			user_from_local_storage.id,
			user_from_local_storage.remember_token
		);
		getTotalBetcSale(
			user_from_local_storage.id,
			user_from_local_storage.remember_token
		);
		getUserAllCryptoTransactions(user_from_local_storage.remember_token);
		getUserStats(user_from_local_storage.remember_token);

		const myResponse = [];
		if (
			Object.keys(user_from_local_storage).length !== 0 &&
			user_from_local_storage.constructor === Object
		) {
			retrieveBetData(
				user_from_local_storage.id,
				user_from_local_storage.remember_token
			)
				.then(
					axios.spread((...response) => {
						myResponse.push(response[0].data);
						// for (let i = 0; i <= 1; i += 1) {
						// 	myResponse.push(response[i].data);
						// }
						//console.log(response)

						if (myResponse[0].results.length != 0) {
							set_bets_completed(myResponse[0].results);
							set_bets_completed_p(myResponse[0].results);
							const data = {
								// req_per_page: document.getElementById( "bets_completed_req_per_page" ).value,
								page_no: 1,
							};
							bets_completed_pagination(
								data,
								myResponse[0].results
							);
						}

						// set_total_investment_earned(myResponse[1].results);
					})
				)
				.finally(() => {
					// Update refreshToken after 3 seconds so this event will re-trigger and update the data
					//setTimeout(() => setRefreshToken(Math.random()), 15000);
				});
		}
	}, [router.isReady, refreshToken]);

	function getUserFromLocalStorage() {
		return JSON.parse(localStorage.getItem("op_user")) || [];
	}
	const jsonBetsArrayFilterByTimeline = (arr, timeline) => {
		return arr.filter(function (arr_v) {
			return arr_v.timeline == timeline;
		});
	};
	async function retrieveBetData(user_id, remember_token) {
		const http = rateLimit(
			axios.create({
				headers: {
					Accept: "application/json",
					Authorization: "Bearer " + remember_token,
				},
			}),
			{ maxRequests: 5, perMilliseconds: 1000, maxRPS: 5 }
		);
		const res = await axios.all([
			/* 
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=0`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/bets?timeline=1`), */
			http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/transactions`),
			// http.post(
			// 	`${process.env.NEXT_PUBLIC_API_DOMAIN}user/total_investment_earned`
			// ),
			/* ,
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/games?timeline=0`),
            http.post(`${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile`) */
		]);

		return res;
	}
	const swalAutoclose = (title, html, timer, timer_progress_bar = false) => {
		let timerInterval;
		return Swal.fire({
			title: title,
			html: html,
			timer: timer,
			timerProgressBar: timer_progress_bar,
			didOpen: () => {
				Swal.showLoading();
				//const b = Swal.getHtmlContainer().querySelector('b')
				// timerInterval = setInterval(() => {
				// b.textContent = Swal.getTimerLeft()
				// }, 100)
			},
			willClose: () => {
				clearInterval(timerInterval);
			},
		}).then((result) => {
			/* Read more about handling dismissals below */
			if (result.dismiss === Swal.DismissReason.timer) {
				//console.log('I was closed by the timer')
			}
		});
	};

	const change_password_submit = async (event) => {
		event.preventDefault();
		console.log([
			event.target.password.value,
			event.target.password_c.value,
			event.target.password_o.value,
		]);

		var alert_message = "";
		var newLine = "\r\n";
		alert_message += newLine;
		if (event.target.password.value !== event.target.password_c.value) {
			alert_message +=
				"New Password & Confirm new password didn't match. <br>";
			alert_message += newLine;
			Swal.fire({
				icon: "error",
				title: "Oops...",
				//text: alert_message
				html: alert_message,
				//footer: '<a href="">Why do I have this issue?</a>'
			});
			return false;
		}

		if (
			event.target.password.value == "" ||
			event.target.password_c.value == "" ||
			event.target.password_o.value == ""
		) {
			alert_message = "Please Fill These Fields: <br>";
			var newLine = "\r\n";
			alert_message += newLine;
			if (event.target.password.value !== event.target.password_c.value) {
				alert_message +=
					"New Password & Confirm new password didn't match. <br>";
				alert_message += newLine;
				Swal.fire({
					icon: "error",
					title: "Oops...",
					//text: alert_message
					html: alert_message,
					//footer: '<a href="">Why do I have this issue?</a>'
				});
				return false;
			}
			if (event.target.password.value == "") {
				alert_message += "New password can not be left blank. <br>";
				alert_message += newLine;
			}
			if (event.target.password_c.value == "") {
				alert_message +=
					"Confirm new password can not be left blank. <br>";
				alert_message += newLine;
			}
			if (event.target.password_o.value == "") {
				alert_message += "Old password can not be left blank. <br>";
				alert_message += newLine;
			}

			Swal.fire({
				icon: "error",
				title: "Oops...",
				//text: alert_message
				html: alert_message,
				//footer: '<a href="">Why do I have this issue?</a>'
			});
			return false;
		}

		var data = new Object();
		data.password = event.target.password.value;
		data.password_confirmation = event.target.password_c.value;
		data.password_old = event.target.password_o.value;
		var config = {
			method: "post",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/change_password`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + op_user.remember_token,
			},
			data: data,
		};

		axios(config)
			.then(function (response) {
				if (response.data.hasOwnProperty("message")) {
					if (response.data.message == "Authenticated") {
						//console.log(response.data);
						if (
							response.data.message_title ==
							"Password changed successfully"
						) {
							localStorage.setItem(
								"op_user",
								JSON.stringify(response.data.user)
							);
							set_op_user(response.data.user);
							swalAutoclose(
								"success",
								"Password changed successfully",
								1000
							);
						} else {
							Swal.fire("Unable to change password");
						}
					} else {
						swalAutoclose(
							"error",
							"Unable to change password",
							1000
						);
					}
				} else {
					swalAutoclose("error", "Unable to change password", 1000);
				}
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to change password", 1000);
			});
	};

	const load_file = async (event) => {
		var image = document.getElementById("output");
		//console.log(event.target.files);
		if (event.target.files.length != 0) {
			image.src = URL.createObjectURL(event.target.files[0]);

			// var data = new Object();
			// data.image = event.target.files[0];
			const data = new FormData();
			data.append("image", event.target.files[0]);
			data.append("name", op_user.name);
			var config = {
				method: "post",
				url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile_update`,
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					Authorization: "Bearer " + op_user.remember_token,
				},
				data: data,
			};

			axios(config)
				.then(function (response) {
					if (response.data.hasOwnProperty("message")) {
						if (response.data.message == "Authenticated") {
							//console.log(response.data);
							if (
								response.data.message_title ==
								"User profile updated successfully"
							) {
								set_op_user(response.data.user);
								swalAutoclose(
									"success",
									"Profile pic updated successfully",
									1000
								);
							} else {
								Swal.fire("Unable to update profile pic");
							}
						} else {
							swalAutoclose(
								"error",
								"Unable to update profile pic",
								1000
							);
						}
					} else {
						swalAutoclose(
							"error",
							"Unable to update profile pic",
							1000
						);
					}
				})
				.catch(function (error) {
					swalAutoclose(
						"error",
						"Unable to update profile pic",
						1000
					);
				});
		}
	};

	const paginator = (items, current_page, per_page_items) => {
		//function paginator(items, current_page, per_page_items) {
		//console.log([typeof current_page, typeof per_page_items]);
		let page = current_page || 1,
			per_page = per_page_items || 10,
			offset = (page - 1) * per_page,
			paginatedItems = items.slice(offset).slice(0, per_page_items),
			total_pages = Math.ceil(items.length / per_page);

		return {
			page: page,
			per_page: per_page,
			pre_page: page - 1 ? page - 1 : null,
			next_page: total_pages > page ? page + 1 : null,
			total: items.length,
			total_pages: total_pages,
			data: paginatedItems,
		};
	};
	// Pagination logic implementation
	const bets_completed_pagination = (data, all_data) => {
		$("#bets_completed_pagination").empty();
		if (data.req_per_page !== "ALL") {
			const myarr = paginator(
				all_data,
				data.page_no,
				parseInt(data.req_per_page)
			);
			//console.log(myarr);
			const string_all_data = JSON.stringify(all_data);
			let pager =
				`<a id="prev_link" class="bets_completed_p_btn" data-value='${string_all_data}' data-id="${myarr.pre_page}">&laquo;</a>` +
				`<a class="bets_completed_p_btn" data-value='${string_all_data}'  data-id="1" >1</a>`;

			for (let num = 2; num <= myarr.total_pages; num++) {
				pager += `<a  class="bets_completed_p_btn" data-value='${string_all_data}'  data-id="${num}">${num}</a>`;
			}
			pager += `<a id="next_link"  class="bets_completed_p_btn" data-value='${string_all_data}'  data-id="${myarr.next_page}">&raquo;</a>`;

			$("#bets_completed_pagination").append(pager);

			$("div#bets_completed_pagination a").each(function () {
				if (this.innerHTML == myarr.page) {
					$(this).addClass("active");
				}
			});

			bets_completed_pagination_initialize(
				"bets_completed_p_btn",
				"bets_completed_req_per_page"
			);
			set_bets_completed(myarr.data);
		} else {
			const myarr = paginator(all_data, data.page_no, all_data.length);
			//console.log(myarr);
			const string_all_data = JSON.stringify(all_data);
			set_bets_completed(myarr.data);
		}
	};

	const bets_completed_pagination_initialize = (
		pagination_btn,
		req_per_page_select
	) => {
		document.querySelectorAll("." + pagination_btn).forEach((btn) => {
			btn.addEventListener("click", () => {
				const page_no = btn.getAttribute("data-id");
				if (page_no !== "null") {
					const data = {
						req_per_page:
							document.getElementById(req_per_page_select).value,
						page_no: parseInt(page_no),
					};
					bets_completed_pagination(
						data,
						JSON.parse(btn.getAttribute("data-value"))
					);
				}
			});
		});
	};
	const bets_completed_filter_requests = async (
		bets_completed_p,
		req_per_page_select
	) => {
		const data = {
			req_per_page: document.getElementById(req_per_page_select).value,
			page_no: 1,
		};
		//console.log([bets_completed_p, req_per_page_select]);
		bets_completed_pagination(data, bets_completed_p);
	};

	async function getTotalBetcPurchased(op_user_id, remember_token) {
		var config = {
			method: "get",
			url:
				`${process.env.NEXT_PUBLIC_API_DOMAIN}user/get_crypto_swap_logs/` +
				op_user_id +
				"/" +
				1,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + remember_token,
			},
		};
		await axios(config)
			.then(function (response) {
				set_total_betc_purchased(response.data);
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to fetch betc purchased.", 1000);
			});
	}

	async function getTotalBetcSale(op_user_id, remember_token) {
		var config = {
			method: "get",
			url:
				`${process.env.NEXT_PUBLIC_API_DOMAIN}user/get_crypto_swap_logs/` +
				op_user_id +
				"/" +
				2,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + remember_token,
			},
		};
		await axios(config)
			.then(function (response) {
				set_total_betc_sale(response.data);
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to fetch betc purchased.", 1000);
			});
	}

	async function getUserAllCryptoTransactions(remember_token) {
		var config = {
			method: "get",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/get_user_all_crypto_transactions`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + remember_token,
			},
		};
		await axios(config)
			.then(function (response) {
				console.log("get_user_all_crypto_transactions");
				console.log(response);
				set_all_crypto_transactions(response.data);
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to fetch betc purchased.", 1000);
			});
	}
	async function getUserStats(remember_token) {
		var config = {
			method: "get",
			url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/get_user_stats`,
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				Authorization: "Bearer " + remember_token,
			},
		};
		await axios(config)
			.then(function (response) {
				set_total_investment(response.data.total_investment);
				set_total_earned(response.data.total_earned);
			})
			.catch(function (error) {
				swalAutoclose("error", "Unable to fetch betc purchased.", 1000);
			});
	}

	return (
		<>
			<Header /* data={op_user} */ />

			<Navbar /* data={op_user} */ />
			{/*header */}
			<section>
				<div className="container-fluid profile-bg">
					<div className="row">
						<div className="col-md-3  text-center py-3 profile-section">
							{/* <i className="fa fa-user-circle profile-icn" aria-hidden="true"></i> */}

							<div className="profile-pic">
								<label className="-label" htmlFor="file">
									<span className="fa-solid fa-camera-retro "></span>
									<span>Change Image</span>
								</label>
								<input
									id="file"
									type="file"
									onChange={() => load_file(event)}
								/>
								<img
									src={
										op_user?.image !== null
											? `${process.env.NEXT_PUBLIC_FILE_PATH}Users_profile_photo/${op_user?.image}`
											: "/images/profile_1584621964.png"
									}
									id="output"
									width="200"
								/>
							</div>
							<h3 className="profile-name">
								{op_user && op_user.name}
							</h3>
							<p className="profile-id">
								{" "}
								ID # {op_user && op_user.id}
							</p>
						</div>
						<div className="col-md-7">
							<div className="row mt-5 mb-3">
								<div className="col-md-6">
									<span className="investment-icon">
										<i className="fa-solid fa-sack-dollar"></i>
									</span>
									<h3 className="investment">
										Total investment
									</h3>
									<span className="amount-investement">
										${" "}
										{total_investment &&
											total_investment.toFixed(2)}
									</span>
								</div>
								<div className="col-md-6">
									<span className="investment-icon">
										<i className="fa-solid fa-hand-holding-dollar"></i>
									</span>
									<h3 className="investment">Total Earned</h3>
									<span className="amount-investement">
										${" "}
										{total_earned &&
											total_earned.toFixed(2)}
									</span>
								</div>
							</div>
						</div>
						<div className="col-md-2 my-wallet text-center">
							<MyWallet></MyWallet>

							<p className="change-password-button mt-3">
								<a
									href="#"
									className="change-password-btn"
									data-toggle="modal"
									data-target="#change_password_modal"
								>
									Change password
								</a>
							</p>
						</div>
					</div>
				</div>
				<div className="container-fluid  profile-section-bg pb-5">
					<div className="row my-profile-section">
						<div className="col-md-6 col-6 my-profile-section text-center py-3">
							<p className="profile-section-activites">
								<Link href="/dashboard">Recent Activity</Link>
							</p>
						</div>
						<div className="col-md-6 col-6 text-center py-3 my-profile-section ">
							<p className="profile-section-bank-roles">
								<Link href="/transactions">Transactions</Link>
							</p>
						</div>
					</div>

					<div className="row">
						<div className="col-md-12 text-center">
							<p className="History-Bets">Transactions</p>
						</div>
					</div>
					<div className="card mx-auto total-transaction my-5">
						<div className="card-body">
							<div className="row ">
								<div className="col-md-4">
									<p className=" From-bank text-center">
										From
									</p>
									<span className="d-flex justify-content-center">
										<img
											src="images/solana-sol-logo.png"
											alt=""
											className="SOL-logo"
										/>
										<h4 className="logo-text mx-2">SOL</h4>
									</span>
								</div>
								<div className="col-md-8">
									<div className="row">
										<div className="col-md-6">
											<h6 className="Total Amount">
												SOL
											</h6>
										</div>
										<div className="col-md-6">
											<h6 className="Coins-Purchased">
												BETCoin
											</h6>
										</div>
									</div>
									{total_betc_purchased.map(
										(
											betc_purchased_row_v,
											betc_purchased_row_i
										) => (
											<div
												key={betc_purchased_row_i}
												className="row"
											>
												<div className="col-md-6">
													<p className="text-center text-white amount-dash">
														{
															betc_purchased_row_v.amount_from
														}{" "}
														SOL
													</p>
												</div>
												<div className="col-md-6">
													<p className="text-center text-white amount-dash">
														{
															betc_purchased_row_v.amount_to
														}{" "}
														BETC
													</p>
												</div>
											</div>
										)
									)}
								</div>
							</div>
						</div>
					</div>

					<div className="card mx-auto total-transaction my-5">
						<div className="card-body">
							<div className="row ">
								<div className="col-md-4">
									<p className=" From-bank text-center">
										From
									</p>
									<span className="d-flex justify-content-center">
										<img
											src="images/bet-coin-logo.png"
											alt=""
											className="SOL-logo"
										/>
										<h4 className="logo-text mx-2">BETC</h4>
									</span>
								</div>
								<div className="col-md-8">
									<div className="row">
										<div className="col-md-6">
											<h6 className="Total Amount">
												BETCoin
											</h6>
										</div>
										<div className="col-md-6">
											<h6 className="Coins-Purchased">
												SOL
											</h6>
										</div>
									</div>
									{total_betc_sale.map(
										(betc_sale_row_v, betc_sale_row_i) => (
											<div
												key={betc_sale_row_i}
												className="row"
											>
												<div className="col-md-6">
													<p className="text-center text-white amount-dash">
														{
															betc_sale_row_v.amount_from
														}{" "}
														BETC
													</p>
												</div>
												<div className="col-md-6">
													<p className="text-center text-white amount-dash">
														{
															betc_sale_row_v.amount_to
														}{" "}
														SOL
													</p>
												</div>
											</div>
										)
									)}
								</div>
							</div>
						</div>
					</div>

					<div className="row usr-transaction-history">
						<div className="col-md-12">
							<div className="row History-Bets-bg pt-3">
								<div className="col-xl-1 col-1 col-sm-1">
									<p className="font-weight-bold History-Bets-desc">
										#
									</p>
								</div>
								<div className="col-xl-3 col-3 col-sm-3">
									<p className="font-weight-bold History-Bets-price">
										Amount
									</p>
								</div>
								<div className="col-xl-4 col-4 col-sm-4">
									<p className="font-weight-bold History-Bets-type">
										Transaction Type
									</p>
								</div>
								<div className="col-xl-4 col-4 col-sm-4">
									<p className="font-weight-bold History-Bets-end-time">
										Created At
									</p>
								</div>
							</div>
						</div>
						{all_crypto_transactions.map(
							(
								crypto_transaction_row_v,
								crypto_transaction_row_i
							) => (
								<div
									key={crypto_transaction_row_i}
									className="col-md-12"
								>
									<div className="row pt-3">
										<div className="col-xl-1 col-1 col-sm-1">
											<p className="History-Bets-desc">
												{crypto_transaction_row_i + 1}
											</p>
										</div>
										<div className="col-xl-3 col-3 col-sm-3">
											<p
												className="History-Bets-price"
												dangerouslySetInnerHTML={{
													__html: crypto_transaction_row_v.formatted_amout,
												}}
											></p>
										</div>
										<div className="col-xl-4 col-4 col-sm-4">
											<p className="History-Bets-type">
												{
													crypto_transaction_row_v.transaction_type_name
												}
											</p>
										</div>
										<div className="col-xl-4 col-4 col-sm-4">
											<p className="History-Bets-end-time">
												{
													crypto_transaction_row_v.formatted_date
												}
											</p>
										</div>
									</div>
								</div>
							)
						)}
					</div>
				</div>

				{/* <!-- change passsword model --> */}
				<div
					className="modal fade betNowModal"
					id="change_password_modal"
					tabIndex="-1"
					role="dialog"
					aria-labelledby="exampleModalCenterTitle"
					aria-hidden="true"
				>
					<div
						className="modal-dialog modal-dialog-centered"
						role="document"
					>
						<div className="modal-content">
							<div className="modal-header">
								<h5
									className="modal-title"
									id="exampleModalLongTitle"
								>
									Change password
								</h5>
								<button
									type="button"
									className="close"
									data-dismiss="modal"
									aria-label="Close"
								>
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form onSubmit={change_password_submit}>
								<div className="modal-body">
									<div className="row">
										<div className="col-md-12">
											<div className="form-group">
												<label htmlFor="password">
													Enter New Password
												</label>
												<input
													type="password"
													className="form-control"
													name="password"
													id="password"
													placeholder="Enter new password"
												/>
											</div>
										</div>
										<div className="col-md-12">
											<div className="form-group">
												<label htmlFor="password_c">
													Confirm New Password
												</label>
												<input
													type="password"
													className="form-control"
													name="password_c"
													id="password_c"
													placeholder="Confirm new password"
												/>
											</div>
										</div>
										<div className="col-md-12">
											<div className="form-group">
												<label htmlFor="password_o">
													Enter Old Password
												</label>
												<input
													type="password"
													className="form-control"
													name="password_o"
													id="password_o"
													placeholder="Enter old password"
												/>
											</div>
										</div>
									</div>
								</div>
								<div className="modal-footer">
									<button
										type="submit"
										className="placebet_btn"
									>
										Save
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				{/* <!-- //change passsword model --> */}
			</section>
			<Footer />
		</>
	);
}
export default my_profile;
