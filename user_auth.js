import axios from "axios";

let UserAuth =class {
    constructor(/* router */) {
        //this.router = router;
    }

    UserCheckAuth({router}) {
        var res_ad = JSON.parse(localStorage.getItem('op_user')) || {};
        if(Object.keys(res_ad).length !== 0 && res_ad.constructor === Object){
            var data = new Object();
            var config = {
                method: 'post',
                url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile`,
                headers: { 
                    'Accept': 'application/json', 
                    'Authorization': 'Bearer '+res_ad.remember_token
                    //...data.getHeaders()
                },
                data : data
            };
    
            axios(config)
            .then(function (response) {
                if(response.data.hasOwnProperty('message')){
                    if(response.data.message == 'Authenticated'){
                        if(Object.keys(response.data.user).length !== 0 && response.data.user.constructor === Object){
                            //console.log(response.data);
                            //router.push("/admin/dashboard");
                        }else{
                            router.push("/login");
                        }
                    }else{
                        router.push("/login");
                    }
                }else{
                    router.push("/login");
                }
            })
            .catch(function (error) {
                router.push("/login");
            });
        }else{
            router.push("/login");
        }
    }

    UserCheckAuthLogin({router}) {
        var res_ad = JSON.parse(localStorage.getItem('op_user')) || {};
        if(Object.keys(res_ad).length !== 0 && res_ad.constructor === Object){
            var data = new Object();
            var config = {
                method: 'post',
                url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile`,
                headers: { 
                    'Accept': 'application/json', 
                    'Authorization': 'Bearer '+res_ad.remember_token
                    //...data.getHeaders()
                },
                data : data
            };
    
            axios(config)
            .then(function (response) {
                if(response.data.hasOwnProperty('message')){
                    if(response.data.message == 'Authenticated'){
                        if(Object.keys(response.data.user).length !== 0 && response.data.user.constructor === Object){
                            //console.log(response.data);
                            router.push("/dashboard");
                        }else{
                            //router.push("/admin/login");
                        }
                    }else{
                        //router.push("/admin/login");
                    }
                }else{
                    //router.push("/admin/login");
                }
            })
            .catch(function (error) {
                //router.push("/admin/login");
            });
        }else{
            //router.push("/admin/login");
        }
        
        
        
    }


    UserCheckAuthRegister({router}) {
        var res_ad = JSON.parse(localStorage.getItem('op_user')) || {};
        if(Object.keys(res_ad).length !== 0 && res_ad.constructor === Object){
            var data = new Object();
            var config = {
                method: 'post',
                url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/profile`,
                headers: { 
                    'Accept': 'application/json', 
                    'Authorization': 'Bearer '+res_ad.remember_token
                    //...data.getHeaders()
                },
                data : data
            };
    
            axios(config)
            .then(function (response) {
                if(response.data.hasOwnProperty('message')){
                    if(response.data.message == 'Authenticated'){
                        if(Object.keys(response.data.user).length !== 0 && response.data.user.constructor === Object){
                            //console.log(response.data);
                            router.push("/dashboard");
                        }else{
                            //router.push("/admin/login");
                        }
                    }else{
                        //router.push("/admin/login");
                    }
                }else{
                    //router.push("/admin/login");
                }
            })
            .catch(function (error) {
                //router.push("/admin/login");
            });
        }else{
            //router.push("/admin/login");
        }
        
        
        
    }       

    UserLogout({router}) {
        var res_ad = JSON.parse(localStorage.getItem('op_user')) || {};
        localStorage.removeItem('op_user');
        if(Object.keys(res_ad).length !== 0 && res_ad.constructor === Object){
            var data = new Object();
            var config = {
                method: 'post',
                url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/logout`,
                headers: { 
                    'Accept': 'application/json', 
                    'Authorization': 'Bearer '+res_ad.remember_token
                    //...data.getHeaders()
                },
                data : data
            };
    
            axios(config)
            .then(function (response) {
                if(response.data.hasOwnProperty('message')){
                    if(response.data.message == 'Authenticated'){
                        //console.log(response.data);
                        router.push("/login");
                    }else{
                        router.push("/login");
                    }
                }else{
                    router.push("/login");
                }
            })
            .catch(function (error) {
                router.push("/login");
            });
        }else{
            router.push("/login");
        }
    }

    test() {
        console.log('test');
    }
    
}
 
module.exports = UserAuth;