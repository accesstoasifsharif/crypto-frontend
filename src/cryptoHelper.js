import {
	getOrCreateAssociatedTokenAccount,
	createTransferCheckedInstruction,
} from "@solana/spl-token";
import axios from "axios";

import { PublicKey, Transaction, LAMPORTS_PER_SOL } from "@solana/web3.js";

export const tokenTrnxOnBetPlace = async (payload) => {
	try {
		const mint = new PublicKey(payload.walletReducer.tokenMintAddress);
		const fromTokenAccount = await getTokenAccountAddress(
			payload.connection,
			payload.publicKey,
			mint,
			payload.publicKey
		);
		const toTokenAccount = await getTokenAccountAddress(
			payload.connection,
			payload.publicKey,
			mint,
			payload.walletReducer.opSportWallet.publicKey
		);

		let tokenTransaction = new Transaction().add(
			createTransferCheckedInstruction(
				fromTokenAccount.address, // from (should be a token account)
				mint, // mint
				toTokenAccount.address, // to (should be a token account)
				payload.publicKey, // from's owner
				payload.amount * LAMPORTS_PER_SOL, // amount,
				9
			)
		);

		const tokenTrnxSignature = await payload.sendTransaction(
			tokenTransaction,
			payload.connection
		);
		const confirmTrnx = await payload.connection.confirmTransaction(
			tokenTrnxSignature,
			"processed"
		);

		if (confirmTrnx && tokenTrnxSignature) {
			let tokenTrnxPayload = {
				transaction_id: tokenTrnxSignature,
				transaction_type: 3,
				sending_wallet_address: payload.publicKey.toBase58(),
				receiving_wallet_address:
					payload.walletReducer.opSportWallet.publicKey.toBase58(),
				amount: payload.amount + "",
				currency: payload.walletReducer.BETCCurrencyID,
			};
			saveCryptoTransactionLog(tokenTrnxPayload, payload.opUser);
			return true;
		}
	} catch (error) {
		console.log(error);
		return false;
	}
	return false;
};

const saveCryptoTransactionLog = async (payload, authUser) => {
	var config = {
		method: "post",
		url: `${process.env.NEXT_PUBLIC_API_DOMAIN}user/save_crypto_transaction`,
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
			Authorization: "Bearer " + authUser.remember_token,
		},
		data: payload,
	};
	axios(config)
		.then(function (response) {})
		.catch(function (error) {
			console.log("saveCryptoTransactionLog", error);
		});
};

export const getTokenAccountAddress = async (
	connection,
	fromWallet,
	mint,
	publicKey
) => {
	const fromTokenAccount = await getOrCreateAssociatedTokenAccount(
		connection,
		fromWallet,
		mint,
		publicKey
	);
	return fromTokenAccount;
};

export const getSwapFeeAmount = (amount, percentage) => {
	let calculatedAmount = ((amount / 100) * percentage).toFixed(9);
	return calculatedAmount;
};
