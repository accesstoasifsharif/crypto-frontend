/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	env: {
		NEXT_PUBLIC_API_DOMAIN: process.env.API_DOMAIN,
		NEXT_PUBLIC_BACK_DOMAIN: process.env.BACK_DOMAIN,
    	NEXT_PUBLIC_CRYPTO_JS_SECRET_KEY:"OpsportBookSec",
    	NEXT_PUBLIC_FILE_PATH: process.env.FILE_PATH
	},
	images: { loader: "custom" },
	publicRuntimeConfig: {
		MINT_TOKEN_ADDRESS: process.env.MINT_TOKEN_ADDRESS,
		WALLET_SECRET_KEY: process.env.WALLET_SECRET_KEY,
		SWAP_FEE_PERCENTAGE: process.env.SWAP_FEE_PERCENTAGE,
	},
};
module.exports = nextConfig;
