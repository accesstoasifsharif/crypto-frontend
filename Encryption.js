import AES from 'crypto-js/aes';
import { enc } from 'crypto-js';
import CryptoJS from 'crypto-js';

let Encryption =class {
    constructor(/* router */) {
        //this.router = router;
    }

    

    encrypt_decrypt(action, string) {
        var output = false;
        var key = CryptoJS.PBKDF2(process.env.NEXT_PUBLIC_CRYPTO_JS_SECRET_KEY, "secret", { keySize: 256/32, iterations: 1000, hasher: CryptoJS.algo.SHA256 }).toString();
        var iv = CryptoJS.PBKDF2(process.env.NEXT_PUBLIC_CRYPTO_JS_SECRET_KEY, "salt", { keySize: 128/32, iterations: 1000, hasher: CryptoJS.algo.SHA256 }).toString();
        if(action == 'encrypt')
        {
            var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(string), CryptoJS.enc.Hex.parse(key), { iv: CryptoJS.enc.Hex.parse(iv) });
            output = encrypted.ciphertext;
        }
        else if(action == 'decrypt')
        {
            var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Hex.parse(string.toString())});
            var decrypted = CryptoJS.AES.decrypt(cipherParams, CryptoJS.enc.Hex.parse(key), { iv: CryptoJS.enc.Hex.parse(iv) });
            output = decrypted.toString(CryptoJS.enc.Utf8);
        }
        return output;
    }

    /* function encrypt_decrypt($action, $string)
    {
        $output = false;
        $method = 'aes-256-cbc';
        // $key = '59b6ab46d379b89d794c87b74a511fbd59b6ab46d379b89d794c87b74a511fbd';
        // $iv = '0aaff094b6dc29742cc98a4bac8bc8f9';
        $key = hash_pbkdf2("sha256", "ERPIntelligentServiceSystem", "secret", 1000, 64);
        $iv = hash_pbkdf2("sha256", "ERPIntelligentServiceSystem", "salt", 1000, 32);
        if($action == 'encrypt')
        {
            $encrypted = openssl_encrypt($string, $method, hex2bin($key), 0, hex2bin($iv));
            $output = bin2hex( base64_decode($encrypted));
        }
        else if( $action == 'decrypt' )
        {
            $decrypted = openssl_decrypt(base64_encode(hex2bin($string)), $method, hex2bin($key), 0, hex2bin($iv));
            $output = $decrypted;
        }
        return $output;
    } */

    encryptId(str){
        const ciphertext = AES.encrypt(str, `${process.env.NEXT_PUBLIC_CRYPTO_JS_SECRET_KEY}`);
        return encodeURIComponent(ciphertext.toString());
    }
    
    decryptId(str){
        const decodedStr = decodeURIComponent(str);
        return AES.decrypt(decodedStr, `${process.env.NEXT_PUBLIC_CRYPTO_JS_SECRET_KEY}`).toString(enc.Utf8);
    }

    
}
 
module.exports = Encryption;