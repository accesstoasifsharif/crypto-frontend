import { UPDATE_WALLET_BALANCE } from "../actions/types";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
import { Keypair } from "@solana/web3.js";
const initState = {
	SOLCurrencyID: 1,
	BETCCurrencyID: 2,
	walletBalance: 0,
	tokenMintAddress: publicRuntimeConfig.MINT_TOKEN_ADDRESS,
	walletSecretKey: publicRuntimeConfig.WALLET_SECRET_KEY,
	swapFeePercentage: publicRuntimeConfig.SWAP_FEE_PERCENTAGE,
};

const WalletReducer = (state = initState, action) => {
	const walletPrivateKey = new Uint8Array(
		JSON.parse(publicRuntimeConfig.WALLET_SECRET_KEY)
	);
	const opSportWallet = Keypair.fromSecretKey(walletPrivateKey);

	state = { ...state, opSportWallet: opSportWallet };

	switch (action.type) {
		case UPDATE_WALLET_BALANCE: {
			return { ...state, walletBalance: action.payload };
		}
	}
	return state;
};

export default WalletReducer;
