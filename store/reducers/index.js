import { combineReducers } from "redux";

import walletReducer from "./WalletReducer";
// Combine all reducers.
const appReducer = combineReducers({
	wallet: walletReducer,
});

const rootReducer = (state, action) => {
	return appReducer(state, action);
};

export default rootReducer;
